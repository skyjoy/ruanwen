# 软文批发网
招募运营合伙搭子，
#### 介绍
基于fastadmin开发的插件模块，媒体发稿平台、在线自助发稿、支持在线充值

#### 软件架构
fastadmin插件。tp+mysql项目


#### 安装教程

1.  先安装fastadmin，详见https://www.fastadmin.net/download.html
2.  下载此插件media及数据库sql脚本
3.  配置插件相关参数
![输入图片说明](%E7%AB%99%E7%82%B9%E9%85%8D%E7%BD%AE.png)
4.  会员中心
![输入图片说明](%E8%BD%AF%E6%96%87%E5%8F%91%E5%B8%83%E2%80%94%E4%BC%98%E9%87%87%E4%BC%A0%E5%AA%92.png)

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
