﻿/**
 * Created by Administrator on 2017/12/13 0013.
 */
$().ready(function () {
    //$(".main:eq(0)").hide();
    //$(".main:eq(1)").show();
    if(a !="") {
        $(".main:eq(0)").hide();
        $(".main:eq(1)").show();
    }
    //定时草稿
    setInterval(function() {
        if($(".main:eq(0)").css("display")!="none"){
            layer.msg('定时存稿中，请稍候...', {
                icon: 1
            }, function () {

            });
        }
        $.post(ajaxurl, {
            t:"savedraft",
            type: type,
            title: encodeURIComponent($("#title").val()),
            fromurl: $("#fromurl").val(),
            content: encodeURIComponent(editor_cmscontent.txt.html()),
            fileurl: $("#fileurl").val()
        }, function (data) {
            //var data = eval("(" + html + ")");
            if (data.code) {
                $("#tempmsg").find(".recly").html("<a href='###' onclick='reclycontent(1);'>恢复最近一次</a>");
                $("#tempmsg").find(".msg").html(new Date().toLocaleString() + " 系统定时存稿成功！ <a href='/addons/media/member/drafttemp/user_id/" + uid + "/t/" + type + "' target='_blank'><strong>预览</strong></a>");
            }
        })
    },30000);
    //恢复最近一次
    $.post(ajaxurl,{"t":"draft","type":type},function(data) {
        //var data=eval("("+html+")");
        if (data.code) {
            $("#tempmsg").find(".recly").html("<a href='###' onclick='reclycontent(1);'>恢复最近一次</a>");
        }
    })
    //预加载媒体列表
    setTimeout(_loadmedia,1000);

    //切换新闻媒体/自媒体选项
    $(".main:eq(1) .tabs-bigtitle").find("li").click(function () {
        page = 1;
        $(".main:eq(1) .tabs-bigtitle").find("li").removeClass("active");
        $(this).addClass("active");
        $("#xinwenmeiti,#zimeiti").hide()
        $("#" + $(this).attr("_rel")).show();
        //
        if ($(this).attr("_rel") == "xinwenmeiti") _loadmedia();
        else _loadmedia2();
    })

    //媒体、套餐、收藏选项卡切换
    var searchbar = $("#xinwenmeiti .tabs-title").find("li");
    searchbar.click(function () {
        searchbar.each(function () {
            $(searchbar).removeClass("active");
        });
        $(this).addClass("active");
        $("#_cfmt").val($(this).attr("data"));
        page = 1;
        _loadmedia();
    })
    //快捷检索
    var channel = $("#channel").find("a");
    channel.click(function () {
        channel.each(function () {
            $(this).removeClass("cur");
        });
        $(this).addClass("cur");
        $("#_channel").val($(this).attr("data"));
        page = 1;
        _select();
        _loadmedia();
    })
    var door = $("#door").find("a");
    door.click(function () {
        door.each(function () {
            $(this).removeClass("cur");
        });
        $(this).addClass("cur");
        $("#_door").val($(this).attr("data"));
        page = 1;
        _select();
        _loadmedia();
    })
    var area = $("#area").find("a");
    area.click(function () {
        area.each(function () {
            $(this).removeClass("cur");
        });
        $(this).addClass("cur");
        $("#_area").val($(this).attr("data"));
        page = 1;
        _select();
        _loadmedia();
    })
    var newssource = $("#newssource").find("a");
    newssource.click(function () {
        newssource.each(function () {
            $(this).removeClass("cur");
        });
        $(this).addClass("cur");
        $("#_newssource").val($(this).attr("data"));
        page = 1;
        _select();
        _loadmedia();
    })

    var position = $("#position").find("a");
    position.click(function () {
        position.each(function () {
            $(this).removeClass("cur");
        });
        $(this).addClass("cur");
        $("#_position").val($(this).attr("data"));
        page = 1;
        _select();
        _loadmedia();
    })

    var linktype = $("#linktype").find("a");
    linktype.click(function () {
        linktype.each(function () {
            $(this).removeClass("cur");
        });
        $(this).addClass("cur");
        $("#_linktype").val($(this).attr("data"));
        page = 1;
        _select();
        _loadmedia();
    })

    var weekfa = $("#weekfa").find("a");
    weekfa.click(function () {
        weekfa.each(function () {
            $(this).removeClass("cur");
        });
        $(this).addClass("cur");
        $("#_weekfa").val($(this).attr("data"));
        page = 1;
        _select();
        _loadmedia();
    })

    var includecondition = $("#includecondition").find("a");
    includecondition.click(function () {
        includecondition.each(function () {
            $(this).removeClass("cur");
        });
        $(this).addClass("cur");
        $("#_includecondition").val($(this).attr("data"));
        page = 1;
        _select();
        _loadmedia();
    })

    var sort = $("#sort").find("a");
    sort.click(function () {
        sort.each(function () {
            $(this).removeClass("cur");
        });
        $(this).addClass("cur");

        var sort_data=$(this).attr("data")
        if($(this).find("i").attr("class")==undefined){
            sort.each(function () {
                $(this).find("i").removeClass();
            });
            $(this).find("i").attr("class","desc")
            sort_data=sort_data+"1";
        }
        else if($(this).find("i").attr("class")=="desc"){
            sort.each(function () {
                $(this).find("i").removeClass();
            });
            $(this).find("i").attr("class","asc")
            sort_data=sort_data+"0";
        }
        else{
            sort.each(function () {
                $(this).find("i").removeClass();
            });
            $(this).find("i").attr("class","desc")
            sort_data=sort_data+"1";
        }
        $("#_sort").val(sort_data);

        page = 1;
        //_select();
        _loadmedia();
    })

    //已选择快捷检索清除
    $("#item_channel").click(function () {
        $("#channel a").first().trigger("click");
    });
    $("#item_door").click(function () {
        $("#door a").first().trigger("click");
    });
    $("#item_area").click(function () {
        $("#area a").first().trigger("click");
    });
    $("#item_newssource").click(function () {
        $("#newssource a").first().trigger("click");
    });
    $("#item_position").click(function () {
        $("#position a").first().trigger("click");
    });
    $("#item_linktype").click(function () {
        $("#linktype a").first().trigger("click");
    });
    $("#item_weekfa").click(function () {
        $("#weekfa a").first().trigger("click");
    });
    //自媒体、收藏选项卡切换
    var searchbar2 = $("#zimeiti .tabs-title").find("li");
    searchbar2.click(function () {
        searchbar2.each(function () {
            $(searchbar2).removeClass("active");
        });
        $(this).addClass("active");
        $("#_cfmt2").val($(this).attr("data"));
        page = 1;
        _loadmedia2();
    })
    //快捷检索
    var platform = $("#platform").find("a");
    platform.click(function () {
        platform.each(function () {
            $(this).removeClass("cur");
        });
        $(this).addClass("cur");
        $("#_platform").val($(this).attr("data"));
        page = 1;
        _select2();
        _loadmedia2();
    })
    var industry = $("#industry").find("a");
    industry.click(function () {
        industry.each(function () {
            $(this).removeClass("cur");
        });
        $(this).addClass("cur");
        $("#_industry").val($(this).attr("data"));
        page = 1;
        _select2();
        _loadmedia2();
    })
    var area2 = $("#area2").find("a");
    area2.click(function () {
        area2.each(function () {
            $(this).removeClass("cur");
        });
        $(this).addClass("cur");
        $("#_area2").val($(this).attr("data"));
        page = 1;
        _select2();
        _loadmedia2();
    })
    var identify = $("#identify").find("a");
    identify.click(function () {
        identify.each(function () {
            $(this).removeClass("cur");
        });
        $(this).addClass("cur");
        $("#_identify").val($(this).attr("data"));
        page = 1;
        _select2();
        _loadmedia2();
    })

    var funs = $("#funs").find("a");
    funs.click(function () {
        funs.each(function () {
            $(this).removeClass("cur");
        });
        $(this).addClass("cur");
        $("#_funs").val($(this).attr("data"));
        page = 1;
        _select2();
        _loadmedia2();
    })

    var read = $("#read").find("a");
    read.click(function () {
        read.each(function () {
            $(this).removeClass("cur");
        });
        $(this).addClass("cur");
        $("#_read").val($(this).attr("data"));
        page = 1;
        _select2();
        _loadmedia2();
    })

    var price = $("#price").find("a");
    price.click(function () {
        price.each(function () {
            $(this).removeClass("cur");
        });
        $(this).addClass("cur");
        $("#_price").val($(this).attr("data"));
        page = 1;
        _select2();
        _loadmedia2();
    })
    //已选择快捷检索清除
    $("#item_platform").click(function () {
        $("#platform a").first().trigger("click");
    });
    $("#item_industry").click(function () {
        $("#industry a").first().trigger("click");
    });
    $("#item_area2").click(function () {
        $("#area2 a").first().trigger("click");
    });
    $("#item_identify").click(function () {
        $("#identify a").first().trigger("click");
    });
    $("#item_funs").click(function () {
        $("#funs a").first().trigger("click");
    });
    $("#item_read").click(function () {
        $("#read a").first().trigger("click");
    });
    $("#item_price").click(function () {
        $("#price a").first().trigger("click");
    });

    //下一步
    $("#btn_step2").click(function () {
        var t = $("#title");
        if (t.val() == "") {
            t.focus();
            t.css({"border": "solid 1px #ff0000"});
            layer.msg('标题不能空！', {
                icon: 1
            }, function () {

            });
            return false;
        }
        t = $("#cmscontent");
        if (t.val() == "" && $("input[name='fileurl']").val() == "") {
            layer.msg('内容不能空！', {
                icon: 1
            }, function () {

            });
            return false;
        }
        //
        // $.post("?filter=true",{"content":$("#cmscontent").val()},function(d){
        //     if(!d.code) {
        //         alert("内容存在敏感关键词：" + d.msg);
        //         $("#filter-div").show();
        //         $("#filter-div td:eq(1)").html("<font color='red'>" + d.msg + "</font>");
        //         return;
        //     }
        //     else{
        //         $("#filter-div").hide();
        //         $('.main:eq(1),#toolbar').show();
        //         $('.main:eq(0)').hide();
        //     }
        // },"json")
        $("#filter-div").hide();
        $('.main:eq(1),#toolbar').show();
        $('.main:eq(0)').hide();
    })
    //保存草稿
    $("#btn_step1").click(function () {
        $.post(ajaxurl, {
            t:"savedraft",
            type: type,
            title: encodeURIComponent($("#title").val()),
            fromurl: $("#fromurl").val(),
            content: encodeURIComponent(editor_cmscontent.txt.html()),
            fileurl: $("#fileurl").val()
        }, function (data) {
            //var data = eval("(" + html + ")");
            if (data.code) {
                $("#btn_step1").html("再次保存");
                $("#tempmsg").find(".recly").html("<a href='###' onclick='reclycontent(1);'>恢复最近一次</a>");
                $("#tempmsg").find(".msg").html(new Date().toLocaleString() + " 系统定时存稿成功！ <a href='/addons/media/member/drafttemp/user_id/" + uid + "/t/" + type + "' target='_blank'><strong>预览</strong></a>");
            }
        })
    })
    //上一步
    $("#btn_back").click(function () {
        $("#filter-div").show();
        $('.main:eq(1),#toolbar').hide();
        $('.main:eq(0)').show();
    })
    //搜索媒体
    $("#btn_search").click(function () {
        page = 1;
        _loadmedia();
    })
    //搜索媒体
    $("#btn_search2").click(function () {
        page = 1;
        _loadmedia2();
    })
    //提交订单
    $("#btn_step3").click(function () {
        var s7 = $("input[name='media']").val();
        var s8=$("input[name='meal']").val();
        var s9=$("input[name='wemedia']").val();
        if ((s7 == "," || s7 == "") && (s8 == "," || s8 == "") && (s9 == "," || s9 == "")) {
            layer.msg('未选择任何媒体或套餐', {
                icon: 1
            }, function(){
                //do something
            });
            return false
        }
        var data = {};

        $("input[name='draftid'],input[name='title'],input[name='fromurl'],input[name='memo'],textarea[name='content'],input[name='fileurl'],input[name='media'],input[name='meal'],input[name='wemedia']").each(function(){
            var name = $(this).attr('name');
            var val =  $(this).val();
            data[ name ] = val ;
        });
        //data['orderbuyid'] = '0';
        $(this).html("请稍候");
        $(this).attr("disabled",true);
        $.post('',data,function(d) {
            layer.msg(d.msg, {
                icon: 1
            }, function(){
                //do something
            });
            if (d.code) {
                window.location.href = d.url;
            }
            $("#btn_step3").html("提交表单");
            $("#btn_step3").removeAttr("disabled");
        },"json")
    })
    //清空选择媒体
    $("#btn_clear").click(function () {
        var checkboxs = $(":checkbox");
        for (var i = 0; i < checkboxs.length; i++) {
            if (checkboxs[i].name == "xz") {
                if (checkboxs[i].checked) {
                    checkboxs[i].click();
                    checkboxs[i].checked = false;
                }
            }
        }
        $("input[name='media'],input[name='meal'],input[name='wemedia']").val(",");
        $("#yxmedia").html("");
        $("#yxmoney").html("0");
    })
    //刷新用户余额
    $(".fa-refresh").click(function () {
        $.get("/index.php/member/home/refresh",function (data) {
            $("#money").html(data);
        })
    })
    //上传
    var bar = $('.bar');
    var percent = $('.percent');
    var showimg = $('#showimg');
    var progress = $(".progress");
    var files = $(".files");
    var btn = $(".upbtn span");
    $("#filedata").wrap("<form id='myupload' action='/api/Common/upload/convert/true' method='post' enctype='multipart/form-data'></form>");
    $("#filedata").change(function () {
        var filepath = $("#filedata").val();
        var extStart = filepath.lastIndexOf(".");
        var ext = filepath.substring(extStart, filepath.length).toUpperCase();
        if (ext != ".DOCX") {
            layer.tips('仅允许上传DOCX格式', '#filedata');
            //alert("仅允许上传DOCX格式")
            return false;
        }
        $("#myupload").ajaxSubmit({
            dataType: 'json',
            beforeSend: function () {
                showimg.empty();
                progress.show();
                var percentVal = '0%';
                bar.width(percentVal);
                percent.html(percentVal);
                $(".percent").show();
                btn.html("上传中...");
                $(".up_tips").html('');
            },
            uploadProgress: function (event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.width(percentVal);
                percent.html(percentVal);
                if (percentComplete == 100) {
                    btn.html("保存中...");
                }
            },
            success: function (data) {
                console.log(data);
                var arr_f=data.data.url.split('/');
                btn.html(arr_f[arr_f.length-1]);
                $(".progress").delay(2000).hide(1);
                $("#filedata").val("");
                $("input[name='fileurl']").val(data.data.url)
                layer.msg(data.msg, {
                    icon: 1
                }, function () {
                    //alert(data.content);
                    // editor_cmscontent.txt.html(data.content);
                    // $textarea_cmscontent.val(editor_cmscontent.txt.html())
                    // return false;
                });
                //$.getScript("https://www.szyjtcm.cn/RemoteConvert.aspx?json=true&url=" + data.data.url.replace(/\\/g, "")+"&s=apinxuan", function () {

                //})
            },
            error: function (xhr) {
                $("#filedata").val("");
                btn.html("上传失败");
                bar.width('0')
                //layer.tips(xhr.responseText, '#filedata');
            }
        });
    });
})

function  reclycontent(t) {
    layer.msg('恢复中，请稍候...', {
        icon: 1
    }, function () {

    });
    $.post(ajaxurl, {"t":"draft","type":t}, function (html) {
        if (html.code) {
            $("#title").val(html.data.title);
            $("#fromurl").val(html.data.fromurl);
            editor_cmscontent.txt.html(html.data.content);
            $textarea_cmscontent.val(editor_cmscontent.txt.html())
            $("#fileurl").val(html.data.fileurl);
        }
    })
}

var page=1;
function _loadmedia() {
    if(page==1){$("#result").html("Loading");}
    $.post(ajaxurl, {
        "t":"media",
        "cfmt": $("#_cfmt").val(),
        "keyword": encodeURIComponent($("#keyword").val()),
        "channel": $("#_channel").val(),
        "door": $("#_door").val(),
        "area": $("#_area").val(),
        "newssource": $("#_newssource").val(),
        "position": $("#_position").val(),
        "linktype": $("#_linktype").val(),
        "weekfa": $("#_weekfa").val(),
        "includecondition":$("#_includecondition").val(),
        "sort":$("#_sort").val(),
        "page": page
    }, function (data) {
        if (parseInt($("#_cfmt").val()) < 4) {
            //媒体
            var tpl = "<table width=\"100%\">"
            tpl += "<thead>"
            tpl += "<tr class=\"f5f5f5\">"
            tpl += "<th class=\"left\" style=\"width: 5%;\">选择</th>"
            tpl += "<th class=\"left\" style=\"width: 8%;\">媒体分类</th>"
            tpl += "<th class=\"left\" style=\"width: 12%;\">媒体名称</th>"
            tpl += "<th style=\"width: 7%;\">地区</th>"
            tpl += "<th style=\"width:7%\">出稿率"
            // tpl += "<span class=\"edit\" >"
            // tpl += "<div class=\"e-box two\">"
            // tpl += "<span class=\"gray-allow\"></span>"
            // tpl += "通过我们长期监测该媒体平台发布平均成功率"
            // tpl += "</div>"
            // tpl += "<img src=\"/assets/addons/media/images/ai-icon-wenhao.png\">"
            // tpl += "</span></th>"
            tpl += "<th style=\"width:6%\">权重"
            tpl += "<span class=\"edit\" >"
            tpl += "<div class=\"e-box two\">"
            tpl += "<span class=\"gray-allow\"></span>"
            tpl += "PC/移动"
            tpl += "</div>"
            tpl += "<img src=\"/assets/addons/media/images/ai-icon-wenhao.png\">"
            tpl += "</span></th>"
            tpl += "<th style=\"width: 7%;\">"
            tpl += "新闻源"
            tpl += "<span class=\"edit\" >"
            tpl += "<div class=\"e-box two\">"
            tpl += "<span class=\"gray-allow\"></span>"
            tpl += "平台标注收录仅供参考，不保证100%收录，需要收录的用户请自行辨别或咨询客服推荐。"
            tpl += "</div>"
            tpl += "<img src=\"/assets/addons/media/images/ai-icon-wenhao.png\">"
            tpl += "</span>"
            tpl += "</th>"
            tpl += "<th style=\"width:7%\">出稿时间"
            tpl += "<span class=\"edit\" >"
            tpl += "<div class=\"e-box two\">"
            tpl += "<span class=\"gray-allow\"></span>"
            tpl += "数据仅供参考"
            tpl += "</div>"
            tpl += "<img src=\"/assets/addons/media/images/ai-icon-wenhao.png\">"
            tpl += "</span>"
            tpl += "</th>"
            tpl += "<th style=\"width: 8%;\">钻石价(我的价格)"
            tpl += "<span class=\"edit\">"
            tpl += "<div class=\"e-box one\">"
            tpl += "<span class=\"gray-allow\"></span>"
            tpl += "在线充值即可自动升级成钻石会员，享受钻石会员价"
            tpl += "</div>"
            tpl += "<img src=\"/assets/addons/media/images/ai-icon-wenhao.png\">"
            tpl += "</span>"

            tpl += "</th>"
            tpl += "<th style=\"width: 10%;\">链接类型</th>"
            tpl += "<th  class=\"left\" style=\"width: 17%;\">"
            tpl += "其他备注"

            tpl += "<span class=\"edit\">"
            tpl += "<div class=\"e-box one\">"
            tpl += "<span class=\"gray-allow\"></span>"
            tpl += "网站的一些特别要求和注意事项"
            tpl += "</div>"
            tpl += "<img src=\"/assets/addons/media/images/ai-icon-wenhao.png\">"
            tpl += "</span>"
            tpl += "</th>"
            tpl += "<th style=\"width: 10%;\">操作</th>"
            tpl += "</tr>"
            tpl += "</thead>"
            tpl += "<tbody>"
            if (page == 1) {
                $("#result").html(tpl)
            }
            tpl = "";
            $.each(data.list, function (i, o) {
                var price = o.memberprice;
                switch (myprice) {
                    case "memberprice":
                        price = o.memberprice
                        break;
                    case "memberprice1":
                        price = o.memberprice1
                        break;
                    case "memberprice2":
                        price = o.memberprice2
                        break;
                    case "memberprice3":
                        price = o.memberprice3
                        break;
                    case "memberprice4":
                        price = o.memberprice4
                        break;
                    default:
                        price = o.memberprice;
                        break;
                }
                tpl += "<tr" + (i % 2 == 0 ? "" : " class=\"f8f8f8\"") + ">"
                tpl += "<td class=\"left\"><i class=\"input-check\" onclick=\"_selmedia(this,'" + o.id + "','" + o.name + "','" + price + "',1)\"></i></td>"
                tpl += "<td class=\"left\">" + o.channel_name + "</td>"
                tpl += "<td class=\"left\"><a style=\"color:blue\" href=\"" + o.example + "\" target=\"_blank\">" + o.name + "</a> <a style=\"color:#fff;background:#ff0000;padding:2px 4px;\" href=\"" + o.example + "\" target=\"_blank\">案例</a>" + (o.includecondition ? " <a style=\"color:#fff;background:#ff9900;padding:2px 4px;cursor:pointer\" title=\"包收录的媒体，若稿件未收录，请在第二个工作日下班前及时提交反馈客服，过期默认收录且不做处理，有特殊要求的媒体请按照媒体备注操作，若周五的订单，在周六日非正常工作日的情况下，可延迟至下周一反馈未收录\">包</a>" : "")+"</td>"
                tpl += "<td>" + o.area_name + "</td>"
                tpl += "<td><div class=\"rateproc\"><div class=\"ratepro\" style=\"width:"+o.publishrate+"%\">&nbsp;</div></div></td>"
                tpl += "<td>" + o.pcweight+"/"+ o.mweight + "</td>"
                tpl += "<td>" + o.newssource_name + "</td>"
                tpl += "<td>" + o.publicationtime_name + "</td>"
                tpl += "<td>" + (myprice == "memberprice4" ? "<font color='red'>" + o.memberprice4 + "</font>" : o.memberprice4) + "元</td>"
                tpl += "<td>" + o.linktype_name + "</td>"
                tpl += "<td class=\"left\">" + o.memo + "</td>"
                tpl += "<td><a href=\"javascript:\" onclick=\"_fav(this," + o.id + ",1);\" op=\"" + ($("#_cfmt").val() != 2 ? "" : "del") + "\"><img src='/assets/addons/media/images/ai-icon-shoucang.png' style='vertical-align: middle' /> " + ($("#_cfmt").val() != 2 ? "收藏" : "取消") + "</a></td>"
                tpl += "</tr>";
            })
            page++;
            $("#result tbody tr:last").remove();
            $("#result tbody").append(tpl);
            tpl = "</tbody>";
            tpl += "</table>";
            $("#result").append(tpl);

            var pagehtml = "<tr><td colspan='12' align='center'><a href='javascript:_loadmedia()' style='height: 35px;background: #efefef;display: block;padding:12px 0 0 0;color:#ff0000;font-size:16px;'>加载更多...</a></td></tr>";
            $("#result tbody").append(pagehtml)
            //
            if (data.list.length == 0) {
                $("#result tbody tr:last td").html("<a style='height: 35px;background: #efefef;display: block;padding:12px 0 0 0;color:#666;font-size:16px;'>亲，没有更多...</a>");
            }
        }
        else {
            //套餐
            var tpl = "<table width=\"100%\">"
            tpl += "<thead>"
            tpl += "<tr class=\"f5f5f5\">"
            tpl += "<th class=\"left\" style=\"width:5%;\">选择</th>"
            tpl += "<th class=\"left\" style=\"width: 15%;\">套餐名称</th>"
            tpl += "<th style=\"width: 7%;\">价格</th>"
            tpl += "<th class=\"left\" style=\"width:63%;\">媒体"
            tpl += "<span class=\"edit\">"
            tpl += "<div class=\"e-box one\">"
            tpl += "<span class=\"gray-allow\"></span>"
            tpl += "单次充值500至1万升级为钻石会员即可享受市场价2.8折优惠"
            tpl += "</div>"
            tpl += "<img src=\"/assets/addons/media/images/ai-icon-wenhao.png\">"
            tpl += "</span>"
            tpl += "</th>"
            tpl += "<th style=\"width: 8%;\">操作</th>"
            tpl += "</tr>"
            tpl += "</thead>"
            tpl += "<tbody>"
            $.each(data.list, function (i, o) {
                tpl += "<tr" + (i % 2 == 0 ? "" : " class=\"f8f8f8\"") + ">"
                tpl += "<td class=\"left\"><i class=\"input-check\" onclick=\"_selmedia(this,'" + o.id + "','" + o.name + "','" + o.money + "',2)\"></i></td>"
                //tpl += "<td class=\"left\"><a href=\"/meal#" + o.id + "\" target=\"_blank\">" + o.name + "</a></td>"
                tpl += "<td class=\"left\">" + o.name + "</td>"
                tpl += "<td>" + o.money + "</td>"
                tpl += "<td class=\"left\">";
                tpl += o.medias + " <strong>备选媒体：" + o.exmedia + "</strong>";
                tpl += "</td>"
                tpl += "<td><a href=\"javascript:\" onclick=\"_fav(this," + o.id + ",2);\" op=\"" + ($("#_cfmt").val() < 5 ? "" : "del") + "\"><img src='/assets/addons/media/images/ai-icon-shoucang.png' style='vertical-align: middle' /> " + ($("#_cfmt").val() < 5 ? "收藏" : "取消") + "</a></td>"
                tpl += "</tr>";
            })
            tpl += "</tbody>";
            tpl += "</table>";
            $("#result").html(tpl);
        }
    },'json');
}

function _select() {
    if ($("#_channel").val() != "不限" && $("#_channel").val() != "") {
        $("#item_channel").html("频道类型：" + $("#channel a[class='cur']").html()+"<em title=\"取消\">X</em>");
        $("#item_channel").show();
    }
    else {
        $("#item_channel").hide();
    }

    if ($("#_door").val() != "不限" && $("#_door").val() != "") {
        $("#item_door").html("综合门户->" + $("#door a[class='cur']").html()+"<em title=\"取消\">X</em>");
        $("#item_door").show();
    }
    else {
        $("#item_door").hide();
    }

    if ($("#_area").val() != "不限" && $("#_area").val() != "") {
        $("#item_area").html("覆盖区域->" + $("#area a[class='cur']").html()+"<em title=\"取消\">X</em>");
        $("#item_area").show();
    }
    else {
        $("#item_area").hide();
    }

    if ($("#_newssource").val() != "不限" && $("#_newssource").val() != "") {
        $("#item_newssource").html("新闻源->" + $("#newssource a[class='cur']").html()+"<em title=\"取消\">X</em>");
        $("#item_newssource").show();
    }
    else {
        $("#item_newssource").hide();
    }

    if ($("#_position").val() != "不限" && $("#_position").val() != "") {
        $("#item_position").html("出现位置->" + $("#position a[class='cur']").html()+"<em title=\"取消\">X</em>");
        $("#item_position").show();
    }
    else {
        $("#item_position").hide();
    }

    if ($("#_linktype").val() != "不限" && $("#_linktype").val() != "") {
        $("#item_linktype").html("链接类型->" + $("#linktype a[class='cur']").html()+"<em title=\"取消\">X</em>");
        $("#item_linktype").show();
    }
    else {
        $("#item_linktype").hide();
    }

    if ($("#_weekfa").val() != "不限" && $("#_weekfa").val() != "") {
        $("#item_weekfa").html("周末->" + $("#weekfa a[class='cur']").html()+"<em title=\"取消\">X</em>");
        $("#item_weekfa").show();
    }
    else {
        $("#item_weekfa").hide();
    }
}

function _loadmedia2() {
    if(page==1){$("#result2").html("Loading");}
    $.post("/index.php/member/wemedia/tojson", {
        "cfmt": $("#_cfmt2").val(),
        "keyword": encodeURIComponent($("#keyword2").val()),
        "platform": $("#_platform").val(),
        "industry": $("#_industry").val(),
        "province": $("#_area2").val(),
        "identify": $("#_identify").val(),
        "funs": $("#_funs").val(),
        "read": $("#_read").val(),
        "price": $("#_price").val(),
        "page": page
    }, function (data) {
        if (parseInt($("#_cfmt2").val()) < 4) {
            //媒体
            var tpl = "<table width=\"100%\">"
            tpl += "<thead>"
            tpl += "<tr class=\"f5f5f5\">"
            tpl += "<th class=\"left\" style=\"width: 4%;\">选择</th>"
            tpl += "<th class=\"left\" style=\"width: 7%;\">平台</th>"
            tpl += "<th class=\"left\" style=\"width: 5%;\">行业</th>"
            tpl += "<th class=\"left\" style=\"width: 15%;\">媒体名称</th>"
            tpl += "<th style=\"width: 5%;\">地区</th>"
            tpl += "<th style=\"width: 5%;\">钻石</th>"
            tpl += "<th style=\"width: 5%;\">认证</th>"
            tpl += "<th style=\"width: 8%;\">粉丝</th>"
            tpl += "<th style=\"width: 8%;\">阅读</th>"
            tpl += "<th class=\"left\" style=\"width: 32%;\">其他备注</th>"
            tpl += "<th style=\"width: 12%;\">操作</th>"
            tpl += "</tr>"
            tpl += "</thead>"
            tpl += "<tbody>"
            if (page == 1) {
                $("#result2").html(tpl)
            }
            tpl = "";
            $.each(data.list, function (i, o) {
                var price = o.memberprice;
                switch (myprice) {
                    case "memberprice":
                        price = o.memberprice
                        break;
                    case "memberprice1":
                        price = o.memberprice1
                        break;
                    case "memberprice2":
                        price = o.memberprice2
                        break;
                    case "memberprice3":
                        price = o.memberprice3
                        break;
                    case "memberprice4":
                        price = o.memberprice4
                        break;
                    default:
                        price = o.memberprice;
                        break;
                }
                tpl += "<tr" + (i % 2 == 0 ? "" : " class=\"f8f8f8\"") + ">"
                tpl += "<td class=\"left\"><i class=\"input-check\" onclick=\"_selmedia2(this,'" + o.id + "','" + o.name + "','" + price + "',1)\"></i></td>"
                tpl += "<td class=\"left\">" + o.platform_name + "</td>"
                tpl += "<td class=\"left\">" + o.industry_name + "</td>"
                tpl += "<td class=\"left\"><font color=\"blue\">" + o.name + "</font> <a style=\"color:#fff;background:#ff0000;padding:2px 4px;\" href=\"" + o.example + "\" target=\"_blank\">案例</a></td>"
                tpl += "<td>" + o.province_name + "</td>"
                tpl += "<td>" + (myprice == "memberprice4" ? "<font color='red'>" + o.memberprice4 + "</font>" : o.memberprice4) + "元</td>"
                tpl += "<td>" + (o.identify ? "已认证" : "未认证") + "</td>"
                tpl += "<td>" + o.funs_name + "</td>"
                tpl += "<td>" + o.read_name + "</td>"
                tpl += "<td class=\"left\">" + o.memo + "</td>"
                tpl += "<td><a href=\"javascript:\" onclick=\"_fav(this," + o.id + ",5);\" op=\"" + ($("#_cfmt2").val() != 2 ? "" : "del") + "\"><img src='/assets/addons/media/images/ai-icon-shoucang.png'> " + ($("#_cfmt2").val() != 2 ? "收藏" : "取消") + "</a></td>"
                tpl += "</tr>";
            })
            page++;
            $("#result2 tbody tr:last").remove();
            $("#result2 tbody").append(tpl);
            tpl = "</tbody>";
            tpl += "</table>";
            $("#result2").append(tpl);

            var pagehtml = "<tr><td colspan='13' align='center'><a href='javascript:_loadmedia2()' style='height: 35px;background: #efefef;display: block;padding:12px 0 0 0;color:#ff0000;font-size:16px;'>加载更多...</a></td></tr>";
            $("#result2 tbody").append(pagehtml)
            //
            if (data.list.length == 0) {
                $("#result2 tbody tr:last td").html("<a style='height: 35px;background: #efefef;display: block;padding:12px 0 0 0;color:#666;font-size:16px;'>亲，没有更多...</a>");
            }
        }
        else {
            //套餐
            // var tpl = "<table class=\"billing-table\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">"
            // tpl += "<thead>"
            // tpl += "<tr>"
            // tpl += "<th width=\"40\">选择</th>"
            // tpl += "<th width=\"180\">套餐名称</th>"
            // tpl += "<th width=\"80\">价格</th>"
            // tpl += "<th>媒体<a href=\"javascript:\" class=\"tip-a\"> <img src=\"/assets/addons/media/images/wenti.png\" height=\"15\" border=\"0\" width=\"15\"><span class=\"outer\"><span class=\"inner\"><p></p><i></i></span></span></a></th>"
            // tpl += "<th>操作</th>"
            // tpl += "</tr>"
            // tpl += "</thead>"
            // tpl += "<tbody>"
            // $.each(data.list, function (i, o) {
            //     tpl += "<tr>"
            //     tpl += "<td><input name=\"xz\" value=\"" + o.id + "\" onclick=\"_selmedia2(this,'" + o.id + "','" + o.name + "','" + o.money + "',2)\" type=\"checkbox\" /></td>"
            //     tpl += "<td height=\"25\" width=\"180\"><a href=\"/meal#" + o.id + "\" target=\"_blank\">" + o.name + "</a></td>"
            //     tpl += "<td>" + o.money + "</td>"
            //     tpl += "<td style=\"text-align:left;line-height: 25px;\">";
            //     // $.each(o.list2, function (i2, o2) {
            //     //     tpl += "<input type=\"checkbox\" value=\"" + o2.id + "\" checked=\"checked\" disabled /> <a href=\"" + o2.example + "\" target=\"_blank\">" + o2.name + "</a> ";
            //     // })
            //     tpl += o.medias + " <strong>备选媒体：" + o.exmedia + "</strong>";
            //     tpl += "</td>"
            //     tpl += "<td><a href=\"javascript:\" onclick=\"_fav(this," + o.id + ",5);\" op=\"" + ($("#_cfmt").val() < 5 ? "" : "del") + "\"><i class=\"fa fa-" + ($("#_cfmt").val() < 5 ? "plus" : "minus") + "-square\"></i> " + ($("#_cfmt").val() < 5 ? "收藏" : "取消") + "</a></td>"
            //     tpl += "</tr>";
            // })
            // tpl += "</tbody>";
            // tpl += "</table>";
            // $("#result2").html(tpl);
        }
    },'json');
}

function _select2() {
    if ($("#_platform").val() != "不限" && $("#_platform").val() != "") {
        $("#item_platform").html("平台->" + $("#platform a[class='cur']").html() + "<em title=\"取消\">X</em>");
        $("#item_platform").show();
    }
    else {
        $("#item_platform").hide();
    }

    if ($("#_industry").val() != "不限" && $("#_industry").val() != "") {
        $("#item_industry").html("行业->" + $("#industry a[class='cur']").html() + "<em title=\"取消\">X</em>");
        $("#item_industry").show();
    }
    else {
        $("#item_industry").hide();
    }

    if ($("#_area2").val() != "不限" && $("#_area2").val() != "") {
        $("#item_area2").html("区域->" + $("#area2 a[class='cur']").html() + "<em title=\"取消\">X</em>");
        $("#item_area2").show();
    }
    else {
        $("#item_area2").hide();
    }

    if ($("#_identify").val() != "不限" && $("#_identify").val() != "") {
        $("#item_identify").html("认证->" + $("#identify a[class='cur']").html() + "<em title=\"取消\">X</em>");
        $("#item_identify").show();
    }
    else {
        $("#item_identify").hide();
    }

    if ($("#_funs").val() != "不限" && $("#_funs").val() != "") {
        $("#item_funs").html("参考粉丝量->" + $("#funs a[class='cur']").html() + "<em title=\"取消\">X</em>");
        $("#item_funs").show();
    }
    else {
        $("#item_funs").hide();
    }

    if ($("#_read").val() != "不限" && $("#_read").val() != "") {
        $("#item_read").html("参考阅读量->" + $("#read a[class='cur']").html() + "<em title=\"取消\">X</em>");
        $("#item_read").show();
    }
    else {
        $("#item_read").hide();
    }

    if ($("#_price").val() != "不限" && $("#_price").val() != "") {
        $("#item_price").html("价 格->" + $("#price a[class='cur']").html() + "<em title=\"取消\">X</em>");
        $("#item_price").show();
    }
    else {
        $("#item_price").hide();
    }
}
/*
type 1=>媒体 2=>套餐
 */
function _selmedia(obj,id,name,price,type) {
    $(obj).toggleClass("check-on");
    if ($(obj).hasClass('check-on')) {
        if (parseFloat($("#money").html()) - parseFloat($("#yxmoney").html()) - parseFloat(price) < 0) {
            //alert('余额不足，请先充值');
            layer.confirm('余额不足，是否前去充值？', {
                btn: ['是', '否'] //按钮
            }, function () {
                window.location.href = "/index.php/member/pay";
            }, function () {
            });
            return false;
        }
        if (type == 1) {
            var yx = $("input[name='media']").val();
            if (yx.indexOf("," + id + ",") > -1) return;
            $("input[name='media']").val($("input[name='media']").val() + id + ",");
        }
        else {
            var yx = $("input[name='meal']").val();
            if (yx.indexOf("," + id + ",") > -1) return;
            $("input[name='meal']").val($("input[name='meal']").val() + id + ",");
        }
        var total2_m = $("#yxmoney").html() * 1 + price * 1;
        $("#yxmoney").html(total2_m.toFixed(2))
        $("#yxmedia").append("<span id='yxmedia2_" + type + "_" + id + "'>" + (type == 2 ? "<font color='#FF9900'>[套]</font>" : "") + name + " " + price + "元<a href='javascript:' onclick=\"_reselmedia('" + id + "'," + price + "," + type + ")\" style='color:#FF0'>×</a></span>");
    }
    else{
        if(type==1){
            if($("input[name='media']").val().indexOf("," + id + ",") > -1){
                _reselmedia(id,price,type)
            }
        }
        else{
            if($("input[name='meal']").val().indexOf("," + id + ",") > -1){
                _reselmedia(id,price,type)
            }
        }
    }
    $("#yxcount").html($("#yxmedia span").length)
}

function _selmedia2(obj,id,name,price,type) {
    $(obj).toggleClass("check-on");
    if ($(obj).hasClass('check-on')) {
        if (parseFloat($("#money").html()) - parseFloat($("#yxmoney").html()) - parseFloat(price) < 0) {
            //alert('余额不足，请先充值');
            layer.confirm('余额不足，是否前去充值？',{
                btn: ['是','否'] //按钮
            },function(){
                window.location.href="/index.php/member/pay";
            },function(){});
            return false;
        }
        if (type == 1) {
            var yx = $("input[name='wemedia']").val();
            if (yx.indexOf("," + id + ",") > -1)return;
            $("input[name='wemedia']").val($("input[name='wemedia']").val() + id + ",");
        }
        else {
            var yx = $("input[name='wemeal']").val();
            if (yx.indexOf("," + id + ",") > -1)return;
            $("input[name='wemeal']").val($("input[name='wemeal']").val() + id + ",");
        }
        var total2_m = $("#yxmoney").html() * 1 + price * 1;
        $("#yxmoney").html(total2_m.toFixed(2))
        $("#yxmedia").append("<span id='yxwemedia2_" + type + "_" + id + "'>" + (type == 2 ? "<font color='#FF9900'>[套]</font>" : "") + name + " " + price + "元<a href='javascript:' onclick=\"_reselmedia2('" + id + "'," + price + "," + type + ")\" style='color:#FF0'>×</a></span>");
    }
    else{
        if(type==1){
            if($("input[name='wemedia']").val().indexOf("," + id + ",") > -1){
                _reselmedia2(id,price,type)
            }
        }
        else{
            if($("input[name='wemeal']").val().indexOf("," + id + ",") > -1){
                _reselmedia2(id,price,type)
            }
        }
    }
    $("#yxcount").html($("#yxmedia span").length)
}

function _reselmedia(id,price,type) {
    if (type == 1) {
        var yx = $("input[name='media']").val();
        yx = yx.replace("," + id + ",", ",");
        $("input[name='media']").val(yx);
    }
    else {
        var yx = $("input[name='meal']").val();
        yx = yx.replace("," + id + ",", ",");
        $("input[name='meal']").val(yx);
    }
    //$("#xz_" + type + "_" + id).attr("checked", false);
    $("#yxmedia2_" + type + "_" + id).remove();	//num.toFixed(2)
    var total3_m = $("#yxmoney").html() * 1 - price * 1;
    $("#yxmoney").html(total3_m.toFixed(2))
    $("#yxcount").html($("#yxmedia span").length)
}

function _reselmedia2(id,price,type) {
    if (type == 1) {
        var yx = $("input[name='wemedia']").val();
        yx = yx.replace("," + id + ",", ",");
        $("input[name='wemedia']").val(yx);
    }
    else {
        var yx = $("input[name='wemeal']").val();
        yx = yx.replace("," + id + ",", ",");
        $("input[name='wemeal']").val(yx);
    }
    //$("#xz_" + type + "_" + id).attr("checked", false);
    $("#yxwemedia2_" + type + "_" + id).remove();	//num.toFixed(2)
    var total3_m = $("#yxmoney").html() * 1 - price * 1;
    $("#yxmoney").html(total3_m.toFixed(2))
    $("#yxcount").html($("#yxmedia span").length)
}
/*
type 1=>媒体 2=>套餐 3=>微信 4=>写手 5=>自媒体
 */
function _fav(obj,id,type) {
    $.post("/index.php/member/home/favorite", {"objectid": id, "type": type, "op": $(obj).attr("op")}, function (data) {
        layer.msg(data.msg, {
            icon: 1
        }, function(){
            if ($(obj).attr("op")=="del") {
                //取消收藏，重新加载数据
                if(type==1){
                    if($("#_cfmt").val()==2 | $("#_cfmt").val()==5){
                        page=1;
                        _loadmedia();
                    }
                    else{
                        $(obj).parent().html("<a href=\"javascript:\" onclick=\"_fav(this," + id + "," + type + ");\" op=\"\"><img src=\"/assets/addons/media/images/ai-icon-shoucang.png\" style=\"vertical-align: middle\">收藏</a>");
                    }
                }
                else{
                    if($("#_cfmt2").val()==2){
                        page=1;
                        _loadmedia2();
                    }
                    else{
                        $(obj).parent().html("<a href=\"javascript:\" onclick=\"_fav(this," + id + "," + type + ");\" op=\"\"><img src=\"/assets/addons/media/images/ai-icon-shoucang.png\" style=\"vertical-align: middle\">收藏</a>");
                    }
                }
            }
            else {
                $(obj).parent().html("<a href=\"javascript:\" onclick=\"_fav(this," + id + "," + type + ");\" op=\"del\">取消</a>");
            }
        });
    }, 'json');
}

function getOs() {
    var OsObject = "";
    if (isIE = navigator.userAgent.indexOf("MSIE") != -1) {
        return "MSIE";
    }
    else return "NOMSIE";
}
function __clearNowrap(obj) {
    var fullnull = "　";
    //if (getOs() == 'NOMSIE') fullnull = "&nbsp;&nbsp;";
    var str = obj.txt.html();

    if (str != "") {
        var reg = new RegExp("<img[^>]*src=\"([^\"]*)\"?[^>]*>", "gi");
        str = str.replace(reg, "==esy==[img]$1[/img]==esy==");
        var reg = new RegExp("<img[^>]*src=\'([^\']*)\'?[^>]*>", "gi");
        str = str.replace(reg, "==esy==[img]$1[/img]==esy==");
        var reg = new RegExp("<hr[^>]*>", "gi");
        str = str.replace(reg, "[hr]");


        //&ldquo;&rdquo;&quot;
        var reg = new RegExp("&ldquo;", "gi");
        str = str.replace(reg, "=ldquo=");
        var reg = new RegExp("&rdquo;", "gi");
        str = str.replace(reg, "=rdquo=");
        var reg = new RegExp("&quot;", "gi");
        str = str.replace(reg, "=quot=");


        str = str.replace("\r", " ");
        str = str.replace("\n", " ");
        str = str.replace("\t", " ");

        reg = new RegExp(fullnull, "gi");
        str = str.replace(reg, "");

        try {

            while (str.substring(0, 5) == "<br/>") {
                str = str.replace("<br/>", "");
            }
            while (str.substring(0, 6) == "<br />") {
                str = str.replace("<br />", "");
            }
        }
        catch (ex) {
        }

        while (str.indexOf("<br/><br/>") != -1) {
            reg = new RegExp("<br[^>]*>( )*<br[^>]*>", "gi");
            str = str.replace(reg, "<br/>");
        }

        reg = new RegExp("(<head>).*(<\/head>)", "gi");
        str = str.replace(reg, "");
        reg = new RegExp("<( )*script([^>])*>", "gi");
        str = str.replace(reg, "<script>");
        reg = new RegExp("(<script>).*(<\/script>)", "gi");
        str = str.replace(reg, "");
        reg = new RegExp("<( )*style([^>])*>", "gi");
        str = str.replace(reg, "<style>");
        reg = new RegExp("(<style>).*(<\/style>)", "gi");
        str = str.replace(reg, "");
        reg = new RegExp("<( )*td([^>])*>", "gi");
        str = str.replace(reg, "");
        reg = new RegExp("<( )*br( )*(/)*>", "gi");
        str = str.replace(reg, "\r\r");
        reg = new RegExp("<( )*li( )*>", "gi");
        str = str.replace(reg, "\r\n\r\n");
        reg = new RegExp("<( )*tr([^>])*>", "gi");
        str = str.replace(reg, "\r\r");
        reg = new RegExp("<( )*p([^>])*>", "gi");
        str = str.replace(reg, "\r\r");
        reg = new RegExp("<[^>]*>", "gi");
        str = str.replace(reg, "");
        reg = new RegExp("\&(.{2,6});", "gi");
        str = str.replace(reg, "");
        reg = new RegExp(" ( )+", "gi");
        str = str.replace(reg, " ");
        reg = new RegExp(" ( )+", "gi");
        str = str.replace(reg, " ");
        reg = new RegExp("(\r)( )+(\r)", "gi");
        str = str.replace(reg, "\r\r");
        reg = new RegExp("(\r\r)+", "gi");
        str = str.replace(reg, "\r\n");


        reg = new RegExp("\n", "gi");
        str = str.replace(reg, "\r\n");
        reg = new RegExp("\r\r", "gi");
        str = str.replace(reg, "\r");
        reg = new RegExp("\r\n\r\n", "gi");
        str = str.replace(reg, "\r\n");

        try {
            while (str.substring(0, 1) == " ") {
                str = str.replace(" ", "");
            }
            while (str.substring(0, 2) == "\r\n") {
                str = str.replace("\r\n", "");
            }
            while (str.substring(0, 1) == "\n") {
                str = str.replace("\n", "");
            }
        }
        catch (ex) {
        }

        while (str.indexOf("\r\n\r\n") != -1) {
            str = str.replace("\r\n\r\n", "\r\n");
        }
        str = str.replace(/\[([^\/\]]*)\]( )*\r\n/gi, "[$1]");
        str = str.replace(/(\r\n)*\[\/([\w]+)\]/gi, "[/$2]");

        str = str.replace(/\[hr\]/gi, "<br/><hr/><br/>" + fullnull + fullnull);
        reg = new RegExp("==esy==( )*\r\n( )*(<br[^>]*>)*<hr/>", "gi");
        str = str.replace(reg, "==esy==<br/><hr/>");
        reg = new RegExp("( )*\r\n( )*(<br[^>]*>)*<hr/>", "gi");
        str = str.replace(reg, "<br/><hr/>");

        //reg = new RegExp("<hr/>[^\r\[]*\r\n","gi");
        //str=str.replace(reg,"<hr/>");
        str = str.replace(/\[\/a\]( )*\r\n( )*\[a ([^\]]*)\]==esy==/gi, "[/a]<br/>[a $3]==esy==");

        reg = new RegExp("\r\n", "gi");
        str = str.replace(reg, "<br/><br/>" + fullnull + fullnull);
        reg = new RegExp("\n", "gi");
        str = str.replace(reg, "<br/><br/>" + fullnull + fullnull);

        //str = str.replace(/\[img\](.+?\.(?:gif|jpg|jpeg|png))\[\/img\]/gi, "<br/><center><img src=\"$1\" border=0></center><br/>");
        str = str.replace(/\[img\](.+?)\[\/img\]/gi, "<br/><center><img src=\"$1\" border=0 style=max-width:600px></center><br/>")
        //str = str.replace(/\[hr\]/gi, "<hr/>");
        str = str.replace(/\[strong\]/gi, "<strong>");
        str = str.replace(/\[\/strong\]/gi, "</strong>");
        str = str.replace(/\[a (.+?)\]/gi, "<a href=\"$1\" target=_blank>");
        str = str.replace(/\[\/a\]/gi, "</a>");

        reg = new RegExp("<a([^>]*)>==esy==(<br[^>]*>)*", "gi");
        str = str.replace(reg, "<br/><a$1>==esy==");
        reg = new RegExp("(<br[^>]*>)*==esy==[^<]*(<br[^>]*>)*[^<]*</a>", "gi");
        str = str.replace(reg, "==esy==</a><br/>");


        try {
            while (str.substring(0, 5) == "<br/>") {
                str = str.replace("<br/>", "");
            }
        }
        catch (ex) {
        }
        if (getOs() == 'MSIE')
            str = fullnull + fullnull + str;

        reg = new RegExp(fullnull + fullnull + "( )*(<a[^>]*>)*( )*==esy==", "gi");
        str = str.replace(reg, "$2==esy==");
        reg = new RegExp("(<br[^>]*>)*==esy==(</a>)*(<br[^>]*>)*(<a[^>]*)*==esy==(<br[^>]*>)*", "gi");
        str = str.replace(reg, "<br/>");
        reg = new RegExp("(<br[^>]*>)*( )*==esy==( )*(<\/a>)( )*(<br[^>]*>)*", "gi");
        str = str.replace(reg, "$4<br/>");
        reg = new RegExp("<br[^>]*>( )*==esy==( )*(<br[^>]*>)*", "gi");
        str = str.replace(reg, "<br/>")
        reg = new RegExp("==esy==", "gi");
        str = str.replace(reg, "");


        try {
            while (str.substring(0, 5) == "<br/>") {
                str = str.replace("<br/>", "");
            }
            while (str.substring(0, 6) == "<br />") {
                str = str.replace("<br />", "");
            }
        }
        catch (ex) {
        }


        while (str.indexOf("<br/><br/><br/>") != -1) {
            str = str.replace("<br/><br/><br/>", " <br/><br/>");
        }

        reg = new RegExp(fullnull + fullnull + "( )*", "gi");
        str = str.replace(reg, fullnull + fullnull);
        reg = new RegExp("(<br[^>]*>)+" + fullnull + fullnull + "(<br[^>]*>)+", "gi");
        str = str.replace(reg, "$1$2");

        var reg = new RegExp("=ldquo=", "gi");
        str = str.replace(reg, "&ldquo;");
        var reg = new RegExp("=rdquo=", "gi");
        str = str.replace(reg, "&rdquo;");
        var reg = new RegExp("=quot=", "gi");
        str = str.replace(reg, "&quot;");

        while (str.lastIndexOf("<br/>") >= (str.length - 7)) {
            str = str.substring(0, str.lastIndexOf("<br/>"));
        }

        //reg = new RegExp("<( )*p([^>])*>", "gi");
        //str = str.replace(reg, "<p>");
        //reg = new RegExp("<p>", "gi");
        //str = str.replace(/\<p>/g, "");
        //reg = new RegExp("</p>", "gi");
        //str = str.replace(/\<\/p>/g, "");
        //alert(str)
        obj.txt.html("<div>" + fullnull + fullnull + str + "</div>");
        //alert("<p>" + fullnull + fullnull + str + "</p>");
        $textarea_cmscontent.val(obj.txt.html())
    }
}
