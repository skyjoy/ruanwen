
// 点击收起与展开按钮
function toggleFilter(){
	$('#toggle-btn').on('click', function(){
		$('#toggle-wrapper').slideToggle(500);
		if($('#toggle-btn').hasClass('on')){
			$('#toggle-btn').text("更多").removeClass('on');
		}else{
			$('#toggle-btn').text("收起").addClass('on');
		}
	});
}

// 侧边栏菜单展开与收起
function Accordion(el, multiple){
    this.el = el || {};
    this.multiple = multiple || false;
    var links = this.el.find('.indicator');

    links.on('click', {
        el: this.el,
        multiple: this.multiple
    }, this.dropdown);
};
Accordion.prototype.dropdown = function(e){
    var $el = e.data.el;
    $this = $(this), $next = $this.next();
    $next.slideToggle();
    $this.parent().toggleClass('on');

    if (!e.data.multiple){
        $el.find('.submenu').not($next).slideUp().parent().removeClass('on');
    }
};

//固定侧边栏
/*$(document).scroll(function() {
	var sTop = $(document).scrollTop();
	if(sTop > 100){
		$('.sidebar').css({top:0});
	}else{
		$('.sidebar').css({top:100 - sTop});
	}
});*/

// 信息价显示层
$('.info-price').hoverIntent({
	over:function(){
		$(this).children('.expire-date').fadeIn();
	},
	out:function(){
		$(this).children('.expire-date').fadeOut();
	}
});

// 推荐商家点击收起
$(document).on('click', '.toggle', function(){
	$(this).parents('.pro-details').slideUp();
});

$(document).ready(function(){
		//顶部公司发展历程特效
		$('.course li').hoverIntent({
			over:function(){
				$(this).children('.bg,.years').animate({opacity:1}, 800);
				$(this).find('.highlight-bottom-line,.highlight-up-line').animate({width:'100%'}, 800);
				$(this).find('.text').fadeIn(800);
			},
			out:function(){
				$(this).children('.bg,.years').animate({opacity:0.5}, 800);
				$(this).find('.highlight-bottom-line,.highlight-up-line').animate({width:0}, 800);
				$(this).find('.text').fadeOut(800);
			}
		});

      
	    // 搜索下拉框
	        $(".select_showbox").click(function(){
			if($(this).next(".select_option").css("display")=="none"){
				$(this).next(".select_option").slideDown(300);
			}else{
				$(this).next(".select_option").slideUp(300);
			}
		})
		$(".select_option li").click(function(){
			$(this).parent().prev(".select_showbox").html($(this).html());
			$(".select_option").slideUp();
		})


		 //全选
		 $(".per-all a").click(function(){
			 if($(this).hasClass("on")){
				 $(this).removeClass("on");
				 $(".all-list li a").removeClass("on");
		 	  }else{
		 		 $(this).addClass("on");
		 		 $(".all-list li a").addClass("on");
		 	  }

 		 });


		 //多选
		 	$(".all-list li a").click(function(e) {
		 	  if($(this).hasClass("on")){
		 		 $(this).removeClass("on");
		 	  }else{
		 		 $(this).addClass("on");
		 	  }

			 /* var whatTab = $(this).index();
			  var howFar = 160 * whatTab;

			  $(".slider").css({
				left: howFar + "px"
			  });
			  $(".ripple").remove();
			  var posX = $(this).offset().left,
				  posY = $(this).offset().top,
				  buttonWidth = $(this).width(),
				  buttonHeight = $(this).height();
			  $(this).prepend("<span class='ripple'></span>");
			  if (buttonWidth >= buttonHeight) {
				buttonHeight = buttonWidth;
			  } else {
				buttonWidth = buttonHeight;
			  }

			  var x = e.pageX - posX - buttonWidth / 2;
			  var y = e.pageY - posY - buttonHeight / 2;
			  $(".ripple").css({
				width: buttonWidth,
				height: buttonHeight,
				top: y + 'px',
				left: x + 'px'
			  }).addClass("rippleEffect");*/
			});

        // 特殊权重设置展开与收起
		$(".toggle-btn-1").click(function(){
				$(this).parent('.data-list').find('.toggle-wrapper1').slideToggle(50);
				if($(this).hasClass('on')){
					$(this).text("点击收起").removeClass('on');
				}else{
					$(this).text("点击展开").addClass('on');
				}
		});



        // 竖版横版切换
		$(".cross-btn").click(function(){
			$(this).addClass('on');
			$('.vertical-btn').removeClass('on');
			$('.page-box').removeClass('nobg-page-box');
			$('.s-data-list').removeClass('o-data-list');
		});

		$(".vertical-btn").click(function(){
			$(this).addClass('on');
			$('.cross-btn').removeClass('on');
			$('.page-box').addClass('nobg-page-box');
			$('.s-data-list').addClass('o-data-list');
		});


		 //供应商多选
		$('body').delegate(".o-data-list dd .bg", 'click', function(){
	         $(this).toggleClass("select");
	         $(this).parent('dd').toggleClass("on");
		});

	    // 自定义单选
	    $(function(){
			$('.all-list-radio a').click(function(){
			var thisToggle = $(this).is('.size_radioToggle') ? $(this) : $(this).prev();
			if(!thisToggle.hasClass("disable")){
				
				var checkBox = thisToggle.prev();
				checkBox.trigger('click');
				$(this).siblings().removeClass('on');
				thisToggle.addClass('on');
			}
			return false;
	    });


	  /*  //新增分类
	  $(".icon-add-classi a").click(function(e) {
		 	   $('.classsi-add').show();
		 	   $('.classsi-add .pop-input').focus();
		});*/
       });

	    //自定义时间
		 $('.custom-img').click(function(e) {
	         $('.search-hide').toggleClass("timeshow");
		 });

});

//扩展数组方法:删除指定元素
Array.prototype.remove = function(val) {
    var index = this.indexOf(val);
    while(index>-1){
        this.splice(index, 1);
        index = this.indexOf(val);
    }
};

// 最近使用功能动画
$('#recent-used').hoverIntent({
	over:function(){
		$(this).children('.recent-wp').animate({paddingLeft:10,opacity:1}, 500).css({display:'block'});
	},
	out:function(){
		$(this).children('.recent-wp').animate({paddingLeft:0,opacity:0}, 500).css({display:'none'});
	}
});

// 工程结构多级展开与收起
function engineToggle(){
	$('.toggle-switch').on('click', function(){
		var elem = $(this).parents('tr').next('.ds').find('.switch-box:first'),
			childrenTag = $(this).children('.add-sub-btn');

		elem.slideToggle();

		childrenTag.toggleClass('open');
	});
}

// 备注
function remarkTxt(){
	$('.remarks-txt').on('click', function(){
		//var txt = $(this).text();通过字符串截取后，应该从title中获取
		var txt = $(this).attr("title")
		$(this).css({display:'none'}).siblings('.remarks-input').css({display:'block'}).val(txt).focus();
	});
	$('.remarks-input').focusout(function(){
		var vals = $(this).val();
		if(!checkNull(vals) && vals.length > 18){
			vals = vals.substring(0,17) + "...";
		}
		if(vals.length == 0){
			$(this).css({display:'none'}).siblings('.remarks-txt').text('备注').css({display:'inline-block'});
		}else{
			$(this).css({display:'none'}).siblings('.remarks-txt').text(vals).css({display:'inline-block'});
		}
	});
}

// 获得焦点显示确定按钮
$(document).on("focusin", ".f-bd-input", function(){
	$(this).siblings('.f-bd-botton').fadeIn();
});

// 失去焦点隐藏确定按钮
$(document).on("focusout", ".f-bd-input", function(){
	$(this).siblings('.f-bd-botton').fadeOut();
});


// 拟建模板展开与收起
function templateToggle(){
	$('.engine-list li > .hd').click(function(){
		$('.engine-list li').removeClass('select');
		$('.engine-list li .bd').slideUp();
		$(this).parents('li').addClass('select');
		if($(this).siblings('.bd').is(':visible')){
			$(this).siblings('.bd').slideUp();
			$(this).parents('li').removeClass('select');
		}else{
			$(this).siblings('.bd').slideDown();
		}
	});
}

// 右侧导航
function rightNav(){
	$.each($('#fixd-nav li'), function(i){
		$(this).bind('click', function(){
			$(this).addClass('on').siblings().removeClass('on');
			$('.engine-list li').removeClass('select');
			$('.engine-list li .bd').slideUp();
			$('.engine-txt-' + (i+1)).addClass('select');
			if($('.engine-txt-' + (i+1)).find('.bd').is(':visible')){
				$('.engine-txt-' + (i+1)).find('.bd').slideUp();
				$('.engine-txt-' + (i+1)).removeClass('select');
			}else{
				$('.engine-txt-' + (i+1)).find('.bd').slideDown();
			}
		});
	});
}

// 进入下一个专业工程拟建
function nextBuild(){
	$('.engine-list .next-project').on('click', function(){
		$(this).parents('.bd').slideUp();
		$(this).parents('li').removeClass('select').next('li').addClass('select').find('.bd').slideDown();
	});
}

// 定时5秒隐藏提示
function hideTips(){
	if($('.project-tips:visible')){
		setTimeout(function(){
			$('.project-tips').fadeOut('slow');
		}, 5000);
	}
}

// 关闭提示
$('.project-tips .close-btn').on('click', function(){
	$(this).parents('.project-tips').fadeOut('slow')
});

// 添加一行
function newRow(){
	$('.add-row-btn').on('click', function(){
		var html = '<tr class="new-row">' +
						'<td class="tal">'+
							'<i class="input-check">'+'</i>'+
						'</td>'+
						'<td>'+'</td>'+
						'<td>'+
							'<input type="text" class="row-input">'+
						'</td>'+
						'<td>'+
							'<input type="text" class="row-input" style="width:130px;">'+
						'</td>'+
						'<td>'+
							'<input type="text" class="row-input">'+
						'</td>'+
						'<td>'+
							'<input type="text" class="row-input">'+
						'</td>'+
						'<td>'+
							'<input type="text" class="row-input">'+
						'</td>'+
						'<td>'+'656'+'</td>'+
						'<td>'+'</td>'+
						'<td>'+'</td>'+
						'<td>'+'</td>'+
						'<td>'+'</td>'+
						'<td>'+'</td>'+
						'<td>'+
							'<p class="remarks-txt ellipsis">'+'备注'+'</p>'+
							'<textarea class="remarks-input">'+'</textarea>'+
						'</td>'+
					'</tr>';

		$(this).siblings('.data-list').find('tr:last').after(html);
		remarkTxt();
	});
}


// 删除内行
function deleteRow(){
	$(document).on('click', '.icon-matrial-delete', function(){
		$(this).parents('.structure-tb-insert tr').fadeOut();
		$.each($('.structure-tb-insert tr'), function(i) {
			$('.structure-tb-insert tr:visible').eq(i-2).find('.new-row-btn').show();
			return false;
		});
	});
}

// 新增内行
function newInsertRow(){
	$(document).on('click', '.new-row-btn', function(){
		if($(this).hasClass('other-btn')){
			var html1 = '<tr>'+
				'<td class="txt-l text-l" width="20%">'+'</td>'+
				'<td width="20%" class="txt-l">'+
					'<select class="select-project">'+
						'<option>'+'人工挑台'+'</option>'+
						'<option>'+'人工挑台'+'</option>'+
						'<option>'+'人工挑台'+'</option>'+
					'</select>'+
					' <a href="javascript:void(0);" class="new-row-btn other-btn" title="新增特征项">'+
					'</a>'+
					'<span class="icon icon-matrial-delete" title="删除">'+
					'</span>'+
				'</td>'
				// +'<td width="15%">'+'</td>'+
				// '<td width="15%">'+'<input type="text" class="row-input">'+'</td>'+
				// '<td width="15%">'+'<input type="text" class="row-input">'+'</td>'+
				// '<td width="15%">'+'<input type="text" class="row-input">'
				// 	+'<span class="icon icon-matrial-delete" title="删除">'
				// 	+'</span>'+
				// '</td>'
			+'</tr>';

			$(this).hide().parents('.structure-tb-insert').find('tr:last').after(html1);
		}else{
			var html2 = '<tr>'+
				'<td width="10%">'+'</td>'+
				'<td width="16%">'+'</td>'+
				'<td width="16%">'+
					'<select class="select-project">'+
						'<option>'+'人工挑台'+'</option>'+
						'<option>'+'人工挑台'+'</option>'+
						'<option>'+'人工挑台'+'</option>'+
					'</select>'+
					' <a href="javascript:void(0);" class="new-row-btn" title="新增特征项">'+
					'</a>'+'<span class="icon icon-matrial-delete" title="删除">'
					+'</span>'+
				'</td>'+
				// '<td width="8%">'+'</td>'+
				// '<td width="10%">'+'<input type="text" class="row-input">'+'</td>'+
				// '<td width="10%">'+'<input type="text" class="row-input">'+'</td>'+
				// '<td width="10%">'+'<input type="text" class="row-input">'+'</td>'+
				// '<td width="8%">'+'</td>'+
				// '<td width="10%">'+'</td>'+
				// '<td width="10%">'
				// 	+'<span class="icon icon-matrial-delete" title="删除">'
				// 	+'</span>'+
				// '</td>'
			+'</tr>';

			$(this).hide().parents('.structure-tb-insert').find('tr:last').after(html2);
		}
		hoverDelete();
	});
}


// 新增内行
function newInsertRows(){
	$(document).on('click', '.new-row-btn', function(){
		if($(this).hasClass('other-btn')){
			var html1 = '<tr>'+
				'<td class="txt-l text-l" width="20%">'+'</td>'+
				'<td width="20%" class="txt-l">'+
					'<select class="select-project">'+
						'<option>'+'人工挑台'+'</option>'+
						'<option>'+'人工挑台'+'</option>'+
						'<option>'+'人工挑台'+'</option>'+
					'</select>'+
					' <a href="javascript:void(0);" class="new-row-btn other-btn" title="新增特征项">'+
					'</a>'+
				'</td>'+
				'<td width="8%">'+'</td>'+
				'<td width="10%">'+'<input type="text" class="row-input">'+'</td>'+
				'<td width="10%">'+'<input type="text" class="row-input">'+'</td>'+
				'<td width="10%">'+'</td>'+
				'<td width="8%">'+'</td>'+
				'<td width="8%">'+'</td>'+
				'<td width="10%">'
				 +'<span class="icon icon-matrial-delete icon-m" title="删除">'
				 +'</span>'+
				 '</td>'
			+'</tr>';

			$(this).hide().parents('.structure-tb-insert').find('tr:last').after(html1);

		}else{
			var html2 = '<tr>'+
				'<td width="10%">'+'</td>'+
				'<td width="16%">'+'</td>'+
				'<td width="16%">'+
					'<select class="select-project">'+
						'<option>'+'人工挑台'+'</option>'+
						'<option>'+'人工挑台'+'</option>'+
						'<option>'+'人工挑台'+'</option>'+
					'</select>'+
					' <a href="javascript:void(0);" class="new-row-btn" title="新增特征项" style="display:inline-block;">'+
					'</a>'+
				'</td>'+
				'<td width="8%">'+'工日'+'</td>'+
				'<td width="10%">'+'<input type="text" class="row-input">'+'</td>'+
				'<td width="10%">'+'<input type="text" class="row-input">'+'</td>'+
				'<td width="10%">'+'</td>'+
				'<td width="8%">'+'</td>'+
				'<td width="8%">'+'</td>'+
				'<td width="10%">'
				 +'<span class="icon icon-matrial-delete icon-m" title="删除">'
				 +'</span>'+
				 '</td>'
			+'</tr>';

			$(this).hide().parents('.structure-tb-insert').find('tr:last').after(html2);
		}
		hoverDelete();
	});
}


// 删除hover事件
function hoverDelete(){
	$('.structure-tb-insert tr').hoverIntent({
		over:function(){
			$(this).find('.icon-matrial-delete').fadeIn();
		},
		out:function(){
			$(this).find('.icon-matrial-delete').fadeOut();
		}
	});
}

// 未归集详细页
function editInput(){
	// 点击.input-txt显示与隐藏下拉框.listing-modify
	$(document).on('click', '.row-input', function(){
		$('.project-tips-icon').hide();
		if($(this).siblings('.listing-modify').is(':hidden')){
			$(this).siblings('.listing-modify').slideDown();
		}else{
			$(this).siblings('.listing-modify').slideUp();
		}
		return false;
	});

	// 显示与隐藏二级菜单
	$('.jmydtree').on('click', function() {
		if($(this).hasClass('on')){
			$(this).removeClass('on');
			$(this).siblings('.dmydtree').slideDown();
			return false;
		}else{
			$(this).addClass('on');
			$(this).siblings('.dmydtree').slideUp();
			return false;
		}
	});

	// 赋值
	$('.dmydtree li a:not(.jmydtree)').on('click', function(){
		var v = $(this).text();
		$(this).parents('.listing-modify').slideUp().siblings('.row-input').val(v);
	});

	// 点击document隐藏显示的下拉框.listing-modify
	if($('.dtreenode:visible')){
		$(document).on('click', function(){
			$('.listing-modify:visible').slideUp();
		});
	}
}
/**
 * 弹出操作等待提示,需要注意字符长度!
 * @param title:标题，默认是： 正在处理中，请稍等...
 * @param content：提示操作内容，默认是：正在处理...
 */
function openOperateWaitTip(title,content){
	if(checkNull(title)){
		title=' 正在处理中，请稍等...';
	}
	if(checkNull(content)){
		content='正在处理...';
	}
	$("#operateWaitTip #title_content").html(title);
	$("#operateWaitTip #popup_content").html(content);
	$("#tipMask").show();
	$("#operateWaitTip").show();
}
/**
 * 关闭操作等待提示
 */
function closeOperateWaitTip(){
	$("#operateWaitTip").hide();
	$("#tipMask").hide();
}



/*// 全选
$('.role-select-wp .check-all').click(function(){
	var allTag = $(this).find('.check-box');

	$(this).find('.check-box').toggleClass('on');

	if(allTag.hasClass('on')){
		$('.role-list li .check-box').addClass('on');
	}else{
		$('.role-list li .check-box').removeClass('on');
	}

	allchk();
});*/
/*
// 单选
$('body').on('click', '.role-list li', function(){
	$(this).find('.check-box').toggleClass('on');

	allchk();
});*/




