define(function(require, exports, module) {
	exports.login = function() {
		require('jquery.form');
		$(".loginBtn").bind('click', function(){
			var _form = $("#front-login"),_account = $('#account'),_pwd = $("#password"),_notice = $('.login-info');
			if(_account.val() == ''){
				_notice.addClass('error').html('请输入用户名/手机号！');
				return false;
			}else{
				_notice.removeClass('error').html('广告主和媒体主都可以直接登录哦.');
			}
			if(_pwd.val() == ''){
				_notice.addClass('error').html('请输入密码！');
				return false;
			}else{
				_notice.removeClass('error').html('广告主和媒体主都可以直接登录哦.');
			}
            var ua = navigator.userAgent.toLowerCase();
            if (ua.match(/MicroMessenger/i) == "micromessenger") {
                $.post($("#front-login").data('action'), {"account": encodeURIComponent($("#account").val()),"password":$("#password").val(),"keeplogin":"on"}, function (r) {
                    if (r.code == 1) {
                        window.location.href = r.url;//fromurl != "" ? fromurl : r.url;
                    } else {
                        _notice.addClass('error').html(r.msg);
                    }
                },"json")
            }
            else {
                _form.ajaxSubmit({
                    type: 'post',
                    url: _form.data('action'),
                    dataType: "json",
                    success: function (r) {
                        if (r.code == 1) {
                            window.location.reload()
                        } else {
                            _notice.addClass('error').html(r.msg);
                        }
                    },
                    error: function (XmlHttpRequest, textStatus, errorThrown) {
                        _notice.addClass('error').html('系统错误,请稍后重试！');
                    }
                });
            }
    		return false;
		});		
	}	
	exports.index = function(){
		$(".mediatar").click(function(){
			var _target = $(this).data('target');
			console.log(_target);
			$(".mediatarget").addClass("targethidden");
			$(".index-media .nav-l ul li").removeClass("cur");
			$(this).addClass("cur");
			$("."+_target).removeClass("targethidden");
			$(".mediatarlink").attr("href", $(this).data("link"));
		});
		$(".catetar").mouseover(function(){
			var _target = $(this).data('target');
			$(".catetarget").addClass("catetargethidden");
			$(".index-media .nav-l-con .nav-t ul li").removeClass("cur");
			$(this).addClass("cur");
			$("."+_target).removeClass("catetargethidden");
		});
	}
	exports.newsPage = function(){
        seajs.use("pagelist",function(p){p.pages('.listContents','/news?request=list')});
        $(".cateSelect a").bind('click', function(){
        	$(".cateSelect a").removeClass('selected');
        	$(this).addClass('selected');
        	var _data = "&category="+$(this).data('value')+
        				//"&price="+$(".priceSelect a.selected").data('price')+
        				"&city="+$(".citySelect a.selected").data('value')+
        				"&protal_type="+$("#protal_type").val()+
        				"&containattr="+$("#containattr").val()+
						"&entryattr="+$("#entryattr").val()+
						"&addattr="+$("#addattr").val();
        	seajs.use("pagelist",function(p){p.pages('.listContents','/news?request=list'+_data)});
            return false;
        });
        $(".priceSelect a").bind('click', function(){
        	$(".priceSelect a").removeClass('selected');
        	$(this).addClass('selected');
        	var _data = "&category="+$(".cateSelect a.selected").data('value')+
        				//"&price="+$(this).data('price')+
        				"&city="+$(".citySelect a.selected").data('value')+
        				"&protal_type="+$("#protal_type").val()+
        				"&containattr="+$("#containattr").val()+
						"&entryattr="+$("#entryattr").val()+
						"&addattr="+$("#addattr").val();
        	seajs.use("pagelist",function(p){p.pages('.listContents','/news?request=list'+_data)});
            return false;
        });
        $(".citySelect a").bind('click', function(){
        	$(".citySelect a").removeClass('selected');
        	$(this).addClass('selected');
        	var _data = "&category="+$(".cateSelect a.selected").data('value')+
        				//"&price="+$(".priceSelect a.selected").data('price')+
        				"&city="+$(this).data('value')+
        				"&protal_type="+$("#protal_type").val()+
        				"&containattr="+$("#containattr").val()+
						"&entryattr="+$("#entryattr").val()+
						"&addattr="+$("#addattr").val();
        	seajs.use("pagelist",function(p){p.pages('.listContents','/news?request=list'+_data)});
            return false;
        });
        //下拉
        $("#addattr").bind('change', function(){
        	var _data = "&category="+$(".cateSelect a.selected").data('value')+
        				//"&price="+$(".priceSelect a.selected").data('price')+
        				"&city="+$(".citySelect a.selected").data('value')+
        				"&protal_type="+$("#protal_type").val()+
        				"&containattr="+$("#containattr").val()+
        				"&entryattr="+$("#entryattr").val()+
        				"&addattr="+$("#addattr").val();
        	seajs.use("pagelist",function(p){p.pages('.listContents','/news?request=list'+_data)});
            return false;
        });
        $("#containattr").bind('change', function(){
        	var _data = "&category="+$(".cateSelect a.selected").data('value')+
        				//"&price="+$(".priceSelect a.selected").data('price')+
        				"&city="+$(".citySelect a.selected").data('value')+
        				"&protal_type="+$("#protal_type").val()+
        				"&containattr="+$("#containattr").val()+
        				"&entryattr="+$("#entryattr").val()+
        				"&addattr="+$("#addattr").val();
        	seajs.use("pagelist",function(p){p.pages('.listContents','/news?request=list'+_data)});
            return false;
        });
        $("#entryattr").bind('change', function(){
        	var _data = "&category="+$(".cateSelect a.selected").data('value')+
        				//"&price="+$(".priceSelect a.selected").data('price')+
        				"&city="+$(".citySelect a.selected").data('value')+
        				"&protal_type="+$("#protal_type").val()+
        				"&containattr="+$("#containattr").val()+
        				"&entryattr="+$("#entryattr").val()+
        				"&addattr="+$("#addattr").val();
        	seajs.use("pagelist",function(p){p.pages('.listContents','/news?request=list'+_data)});
            return false;
        });
        $("#protal_type").bind('change', function(){
        	var _data = "&category="+$(".cateSelect a.selected").data('value')+
        				//"&price="+$(".priceSelect a.selected").data('price')+
        				"&city="+$(".citySelect a.selected").data('value')+
        				"&protal_type="+$("#protal_type").val()+
        				"&containattr="+$("#containattr").val()+
        				"&entryattr="+$("#entryattr").val()+
        				"&addattr="+$("#addattr").val();
        	seajs.use("pagelist",function(p){p.pages('.listContents','/news?request=list'+_data)});
            return false;
        });
        $(".seniorSearch").bind('click', function(){
            var _data = "&category="+$(".cateSelect a.selected").data('value')+
            			"&keywords="+$("#keywords").val()+
						//"&price="+$(".priceSelect a.selected").data('price')+
						"&city="+$(".citySelect a.selected").data('value')+
						"&protal_type="+$("#protal_type").val()+
						"&containattr="+$("#containattr").val()+
						"&entryattr="+$("#entryattr").val()+
						"&addattr="+$("#addattr").val();
            seajs.use("pagelist",function(p){p.pages('.listContents','/news?request=list'+_data)});
            return false;
        });
       /* $(".priceSerach").bind('click', function(){
            var _data = "&category="+$(".cateSelect a.selected").data('value')+
						//"&price="+$(".priceSelect a.selected").data('price')+
						//"&price1="+$("#price1").val()+
						//"&price2="+$("#price2").val()+
						"&city="+$(".citySelect a.selected").data('value')+
						"&protal_type="+$("#protal_type").val()+
						"&containattr="+$("#containattr").val()+
						"&entryattr="+$("#entryattr").val()+
						"&addattr="+$("#addattr").val();
            seajs.use("pagelist",function(p){p.pages('.listContents','/news?request=list'+_data)});
            return false;
        });*/
        $(".clearSearch").bind('click', function(){
        	$(".cateSelect a").removeClass('selected').first('a').addClass('selected');
        	//$(".priceSelect a").removeClass('selected').first('a').addClass('selected');
        	$(".citySelect a").removeClass('selected').first('a').addClass('selected');
        	//$("#price1").val('');
        	//$("#price2").val('');
        	$("#protal_type").val('');
        	$("#containattr").val('');
        	$("#entryattr").val('');
        	$("#addattr").val('');
        	$("#keywords").val('');
            seajs.use("pagelist",function(p){p.pages('.listContents','/news?request=list')});
            return false;
        });
	}
	
	exports.combosPage = function(){
		seajs.use("pagelist",function(p){p.pages('.listContents','/combos?request=list')});
        $(".cateSelect a").bind('click', function(){
        	$(".cateSelect a").removeClass('selected');
        	$(this).addClass('selected');
        	var _data = "&category="+$(this).data('value')+
        				"&price="+$(".priceSelect a.selected").data('value');
        	seajs.use("pagelist",function(p){p.pages('.listContents','/combos?request=list'+_data)});
            return false;
        });
        $(".priceSelect a").bind('click', function(){
        	$(".priceSelect a").removeClass('selected');
        	$(this).addClass('selected');
        	var _data = "&category="+$(".cateSelect a.selected").data('value')+
        				"&price="+$(".priceSelect a.selected").data('value');
        	seajs.use("pagelist",function(p){p.pages('.listContents','/combos?request=list'+_data)});
            return false;
        });
        $(".seniorSearch").bind('click', function(){
            var _data = "&category="+$(".cateSelect a.selected").data('value')+
            		"&keywords="+$("#keywords").val()+
            		"&price="+$(".priceSelect a.selected").data('value');
            seajs.use("pagelist",function(p){p.pages('.listContents','/combos?request=list'+_data)});
            return false;
        });
	}
	
	exports.postList = function(cid){
		if(cid) {
			seajs.use("pagelist",function(p){p.pages('#post_list_box','/post?request=list&cid='+cid)});
		} else {
			seajs.use("pagelist",function(p){p.pages('#post_list_box','/post?request=list')});
		}
	}
	exports.focusPage = function(){
        seajs.use("pagelist",function(p){p.pages('.listContents','/focus?request=list')});
        $(".cateSelect a").bind('click', function(){
        	$(".cateSelect a").removeClass('selected');
        	$(this).addClass('selected');
        	var _data = "&category="+$(this).data('value')+
        				//"&price="+$(".priceSelect a.selected").data('price')+
        				"&city="+$(".citySelect a.selected").data('value')+
        				"&protal_type="+$("#protal_type").val()+
        				"&containattr="+$("#containattr").val()+
						"&entryattr="+$("#entryattr").val()+
						"&addattr="+$("#addattr").val()+
						"&focustime="+$("#focustime").val();
        	seajs.use("pagelist",function(p){p.pages('.listContents','/focus?request=list'+_data)});
            return false;
        });
        $(".priceSelect a").bind('click', function(){
        	$(".priceSelect a").removeClass('selected');
        	$(this).addClass('selected');
        	var _data = "&category="+$(".cateSelect a.selected").data('value')+
        				//"&price="+$(this).data('price')+
        				"&city="+$(".citySelect a.selected").data('value')+
        				"&protal_type="+$("#protal_type").val()+
        				"&containattr="+$("#containattr").val()+
						"&entryattr="+$("#entryattr").val()+
						"&addattr="+$("#addattr").val()+
						"&focustime="+$("#focustime").val();
        	seajs.use("pagelist",function(p){p.pages('.listContents','/focus?request=list'+_data)});
            return false;
        });
        $(".citySelect a").bind('click', function(){
        	$(".citySelect a").removeClass('selected');
        	$(this).addClass('selected');
        	var _data = "&category="+$(".cateSelect a.selected").data('value')+
        				//"&price="+$(".priceSelect a.selected").data('price')+
        				"&city="+$(this).data('value')+
        				"&protal_type="+$("#protal_type").val()+
        				"&containattr="+$("#containattr").val()+
						"&entryattr="+$("#entryattr").val()+
						"&addattr="+$("#addattr").val()+
						"&focustime="+$("#focustime").val();
        	seajs.use("pagelist",function(p){p.pages('.listContents','/focus?request=list'+_data)});
            return false;
        });
        //下拉
        $("#addattr").bind('change', function(){
        	var _data = "&category="+$(".cateSelect a.selected").data('value')+
        				//"&price="+$(".priceSelect a.selected").data('price')+
        				"&city="+$(".citySelect a.selected").data('value')+
        				"&protal_type="+$("#protal_type").val()+
        				"&containattr="+$("#containattr").val()+
        				"&entryattr="+$("#entryattr").val()+
        				"&addattr="+$("#addattr").val()+
						"&focustime="+$("#focustime").val();
        	seajs.use("pagelist",function(p){p.pages('.listContents','/focus?request=list'+_data)});
            return false;
        });
        $("#containattr").bind('change', function(){
        	var _data = "&category="+$(".cateSelect a.selected").data('value')+
        				//"&price="+$(".priceSelect a.selected").data('price')+
        				"&city="+$(".citySelect a.selected").data('value')+
        				"&protal_type="+$("#protal_type").val()+
        				"&containattr="+$("#containattr").val()+
        				"&entryattr="+$("#entryattr").val()+
        				"&addattr="+$("#addattr").val()+
						"&focustime="+$("#focustime").val();
        	seajs.use("pagelist",function(p){p.pages('.listContents','/focus?request=list'+_data)});
            return false;
        });
        $("#entryattr").bind('change', function(){
        	var _data = "&category="+$(".cateSelect a.selected").data('value')+
        				//"&price="+$(".priceSelect a.selected").data('price')+
        				"&city="+$(".citySelect a.selected").data('value')+
        				"&protal_type="+$("#protal_type").val()+
        				"&containattr="+$("#containattr").val()+
        				"&entryattr="+$("#entryattr").val()+
        				"&addattr="+$("#addattr").val()+
						"&focustime="+$("#focustime").val();
        	seajs.use("pagelist",function(p){p.pages('.listContents','/focus?request=list'+_data)});
            return false;
        });
        $("#focustime").bind('change', function(){
        	var _data = "&category="+$(".cateSelect a.selected").data('value')+
        				//"&price="+$(".priceSelect a.selected").data('price')+
        				"&city="+$(".citySelect a.selected").data('value')+
        				"&protal_type="+$("#protal_type").val()+
        				"&containattr="+$("#containattr").val()+
        				"&entryattr="+$("#entryattr").val()+
        				"&addattr="+$("#addattr").val()+
						"&focustime="+$("#focustime").val();
        	seajs.use("pagelist",function(p){p.pages('.listContents','/focus?request=list'+_data)});
            return false;
        });
        $("#protal_type").bind('change', function(){
        	var _data = "&category="+$(".cateSelect a.selected").data('value')+
        				//"&price="+$(".priceSelect a.selected").data('price')+
        				"&city="+$(".citySelect a.selected").data('value')+
        				"&protal_type="+$("#protal_type").val()+
        				"&containattr="+$("#containattr").val()+
        				"&entryattr="+$("#entryattr").val()+
        				"&addattr="+$("#addattr").val()+
						"&focustime="+$("#focustime").val();
        	seajs.use("pagelist",function(p){p.pages('.listContents','/focus?request=list'+_data)});
            return false;
        });
        $(".seniorSearch").bind('click', function(){
            var _data = "&category="+$(".cateSelect a.selected").data('value')+
            			"&keywords="+$("#keywords").val()+
						//"&price="+$(".priceSelect a.selected").data('price')+
						"&city="+$(".citySelect a.selected").data('value')+
						"&protal_type="+$("#protal_type").val()+
						"&containattr="+$("#containattr").val()+
						"&entryattr="+$("#entryattr").val()+
						"&addattr="+$("#addattr").val()+
						"&focustime="+$("#focustime").val();
            seajs.use("pagelist",function(p){p.pages('.listContents','/focus?request=list'+_data)});
            return false;
        });
        /*$(".priceSerach").bind('click', function(){
            var _data = "&category="+$(".cateSelect a.selected").data('value')+
						//"&price="+$(".priceSelect a.selected").data('price')+
						//"&price1="+$("#price1").val()+
						//"&price2="+$("#price2").val()+
						"&city="+$(".citySelect a.selected").data('value')+
						"&protal_type="+$("#protal_type").val()+
						"&containattr="+$("#containattr").val()+
						"&entryattr="+$("#entryattr").val()+
						"&addattr="+$("#addattr").val()+
						"&focustime="+$("#focustime").val();
            seajs.use("pagelist",function(p){p.pages('.listContents','/focus?request=list'+_data)});
            return false;
        });*/
        $(".clearSearch").bind('click', function(){
        	$(".cateSelect a").removeClass('selected').first('a').addClass('selected');
        	//$(".priceSelect a").removeClass('selected').first('a').addClass('selected');
        	$(".citySelect a").removeClass('selected').first('a').addClass('selected');
        	//$("#price1").val('');
        	//$("#price2").val('');
        	$("#protal_type").val('');
        	$("#containattr").val('');
        	$("#entryattr").val('');
        	$("#addattr").val('');
        	$("#keywords").val('');
        	$("#focustime").val('');
            seajs.use("pagelist",function(p){p.pages('.listContents','/focus?request=list')});
            return false;
        });
	}	
	exports.mediaPage = function(){
        seajs.use("pagelist",function(p){p.pages('.listContents','/media?request=list')});
        $(".platformSelect a").bind('click', function(){
        	$(".platformSelect a").removeClass('selected');
        	$(this).addClass('selected');
        	var _data = "&platform="+$(this).data('value')+
        				"&city="+$(".citySelect a.selected").data('value')+
        				"&category="+$(".cateSelect a.selected").data('value')+
        				"&area="+$(".areaSelect a.selected").data('value')+
        				"&addattr="+$("#addattr").val();
        	seajs.use("pagelist",function(p){p.pages('.listContents','/media?request=list'+_data)});
            return false;
        });
        $(".citySelect a").bind('click', function(){
        	$(".citySelect a").removeClass('selected');
        	$(this).addClass('selected');
        	var _data = "&city="+$(this).data('value')+
        				"&platform="+$(".platformSelect a.selected").data('value')+
        				"&category="+$(".cateSelect a.selected").data('value')+
        				"&area="+$(".areaSelect a.selected").data('value')+
        				"&addattr="+$("#addattr").val();
        	seajs.use("pagelist",function(p){p.pages('.listContents','/media?request=list'+_data)});
            return false;
        });
        $("#addattr").bind('change', function(){
        	var _data = "&platform="+$(".platformSelect a.selected").data('value')+
        				"&city="+$(".citySelect a.selected").data('value')+
        				"&category="+$(".cateSelect a.selected").data('value')+
        				"&area="+$(".areaSelect a.selected").data('value')+
        				"&addattr="+$("#addattr").val();
        	seajs.use("pagelist",function(p){p.pages('.listContents','/media?request=list'+_data)});
            return false;
        });
        //下拉
        $(".cateSelect a").bind('click', function(){
        	$(".cateSelect a").removeClass('selected');
        	$(this).addClass('selected');
        	var _data = "&category="+$(this).data('value')+
        				"&city="+$(".citySelect a.selected").data('value')+
        				"&platform="+$(".platformSelect a.selected").data('value')+
        				"&area="+$(".areaSelect a.selected").data('value')+
        				"&addattr="+$("#addattr").val();
        	seajs.use("pagelist",function(p){p.pages('.listContents','/media?request=list'+_data)});
            return false;
        });
        $(".areaSelect a").bind('click', function(){
        	$(".areaSelect a").removeClass('selected');
        	$(this).addClass('selected');
        	var _data = "&area="+$(this).data('value')+
        				"&city="+$(".citySelect a.selected").data('value')+
        				"&category="+$(".cateSelect a.selected").data('value')+
        				"&platform="+$(".platformSelect a.selected").data('value')+
        				"&addattr="+$("#addattr").val();
        	seajs.use("pagelist",function(p){p.pages('.listContents','/media?request=list'+_data)});
            return false;
        });
        $(".seniorSearch").bind('click', function(){
            var _data = "&platform="+$(".platformSelect a.selected").data('value')+
            			"&keywords="+$("#keywords").val()+
						"&city="+$(".citySelect a.selected").data('value')+
						"&category="+$(".cateSelect a.selected").data('value')+
						"&area="+$(".areaSelect a.selected").data('value')+
        				"&addattr="+$("#addattr").val();
            seajs.use("pagelist",function(p){p.pages('.listContents','/media?request=list'+_data)});
            return false;
        });
        $(".clearSearch").bind('click', function(){
        	$(".platformSelect a").removeClass('selected').first('a').addClass('selected');
        	$(".areaSelect a").removeClass('selected').first('a').addClass('selected');
        	$(".cateSelect a").removeClass('selected').first('a').addClass('selected');
        	$(".citySelect a").removeClass('selected').first('a').addClass('selected');
        	$("#keywords").val('');
        	$("#addattr").val('');
            seajs.use("pagelist",function(p){p.pages('.listContents','/media?request=list')});
            return false;
        });
	}	
	exports.wechatPage = function(){
        seajs.use("pagelist",function(p){p.pages('.listContents','/wechat?request=list')});
        $(".citySelect a").bind('click', function(){
        	$(".citySelect a").removeClass('selected');
        	$(this).addClass('selected');
        	var _data = "&city="+$(this).data('value')+
        				"&category="+$(".cateSelect a.selected").data('value')+
        				"&area="+$(".areaSelect a.selected").data('value');
        	seajs.use("pagelist",function(p){p.pages('.listContents','/wechat?request=list'+_data)});
            return false;
        });
        //下拉
        $(".cateSelect a").bind('click', function(){
        	$(".cateSelect a").removeClass('selected');
        	$(this).addClass('selected');
        	var _data = "&category="+$(this).data('value')+
        				"&city="+$(".citySelect a.selected").data('value')+
        				"&area="+$(".areaSelect a.selected").data('value');
        	seajs.use("pagelist",function(p){p.pages('.listContents','/wechat?request=list'+_data)});
            return false;
        });
        $(".areaSelect a").bind('click', function(){
        	$(".areaSelect a").removeClass('selected');
        	$(this).addClass('selected');
        	var _data = "&area="+$(this).data('value')+
        				"&city="+$(".citySelect a.selected").data('value')+
        				"&category="+$(".cateSelect a.selected").data('value');
        	seajs.use("pagelist",function(p){p.pages('.listContents','/wechat?request=list'+_data)});
            return false;
        });
        $(".seniorSearch").bind('click', function(){
            var _data = "&keywords="+$("#keywords").val()+
						"&city="+$(".citySelect a.selected").data('value')+
						"&category="+$(".cateSelect a.selected").data('value')+
						"&area="+$(".areaSelect a.selected").data('value');
            seajs.use("pagelist",function(p){p.pages('.listContents','/wechat?request=list'+_data)});
            return false;
        });
        $(".clearSearch").bind('click', function(){
        	$(".areaSelect a").removeClass('selected').first('a').addClass('selected');
        	$(".cateSelect a").removeClass('selected').first('a').addClass('selected');
        	$(".citySelect a").removeClass('selected').first('a').addClass('selected');
        	$("#keywords").val('');
            seajs.use("pagelist",function(p){p.pages('.listContents','/wechat?request=list')});
            return false;
        });
	}	
	exports.weiboPage = function(){
        seajs.use("pagelist",function(p){p.pages('.listContents','/weibo?request=list')});
        $(".citySelect a").bind('click', function(){
        	$(".citySelect a").removeClass('selected');
        	$(this).addClass('selected');
        	var _data = "&city="+$(this).data('value')+
        				"&category="+$(".cateSelect a.selected").data('value')+
        				"&area="+$(".areaSelect a.selected").data('value');
        	seajs.use("pagelist",function(p){p.pages('.listContents','/weibo?request=list'+_data)});
            return false;
        });
        //下拉
        $(".cateSelect a").bind('click', function(){
        	$(".cateSelect a").removeClass('selected');
        	$(this).addClass('selected');
        	var _data = "&category="+$(this).data('value')+
        				"&city="+$(".citySelect a.selected").data('value')+
        				"&area="+$(".areaSelect a.selected").data('value');
        	seajs.use("pagelist",function(p){p.pages('.listContents','/weibo?request=list'+_data)});
            return false;
        });
        $(".areaSelect a").bind('click', function(){
        	$(".areaSelect a").removeClass('selected');
        	$(this).addClass('selected');
        	var _data = "&area="+$(this).data('value')+
        				"&city="+$(".citySelect a.selected").data('value')+
        				"&category="+$(".cateSelect a.selected").data('value');
        	seajs.use("pagelist",function(p){p.pages('.listContents','/weibo?request=list'+_data)});
            return false;
        });
        $(".seniorSearch").bind('click', function(){
            var _data = "&keywords="+$("#keywords").val()+
						"&city="+$(".citySelect a.selected").data('value')+
						"&category="+$(".cateSelect a.selected").data('value')+
						"&area="+$(".areaSelect a.selected").data('value');
            seajs.use("pagelist",function(p){p.pages('.listContents','/weibo?request=list'+_data)});
            return false;
        });
        $(".clearSearch").bind('click', function(){
        	$(".areaSelect a").removeClass('selected').first('a').addClass('selected');
        	$(".cateSelect a").removeClass('selected').first('a').addClass('selected');
        	$(".citySelect a").removeClass('selected').first('a').addClass('selected');
        	$("#keywords").val('');
            seajs.use("pagelist",function(p){p.pages('.listContents','/weibo?request=list')});
            return false;
        });
	}
});