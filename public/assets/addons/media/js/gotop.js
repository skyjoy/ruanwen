(function($){
    $(function(){
        var upperLimit = 100;   //距离顶部最小距离
        var scrollElem = $('#go-top');  //获取元素
        var scrollSpeed = 500;  //滚动时间

        $(window).scroll(function () {
            var scrollTop = $(document).scrollTop();

            if(scrollTop > upperLimit){
                $(scrollElem).stop().fadeTo(300, 1).css({display:'block'});
            }else{
                $(scrollElem).stop().fadeTo(300, 0).css({display:'none'});
            }
        });

        $(scrollElem).click(function(){
          $('html, body').animate({scrollTop:0}, scrollSpeed); return false;
        });
    });
})(jQuery);