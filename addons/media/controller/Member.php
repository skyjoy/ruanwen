<?php
namespace addons\media\controller;

use app\common\controller\Frontend;
use think\addons\Controller;
use think\Db;
use think\Exception;
use think\Config;
use think\Cookie;
use think\Hook;
use think\Session;
use think\Validate;


class Member extends Controller//Frontend
{
    protected $noNeedLogin = ['init','login', 'register', 'third'];
    protected $noNeedRight = ['*'];

    public function _initialize()
    {
        parent::_initialize();
        $auth = $this->auth;

        if (!Config::get('fastadmin.usercenter')) {
            $this->error(__('User center already closed'), '/');
        }

        //监听注册登录退出的事件
        Hook::add('user_login_successed', function ($user) use ($auth) {
            $expire = input('post.keeplogin') ? 30 * 86400 : 0;
            Cookie::set('uid', $user->id, $expire);
            Cookie::set('token', $auth->getToken(), $expire);
        });
        Hook::add('user_register_successed', function ($user) use ($auth) {
            Cookie::set('uid', $user->id);
            Cookie::set('token', $auth->getToken());
        });
        Hook::add('user_delete_successed', function ($user) use ($auth) {
            Cookie::delete('uid');
            Cookie::delete('token');
        });
        Hook::add('user_logout_successed', function ($user) use ($auth) {
            Cookie::delete('uid');
            Cookie::delete('token');
        });
//        print_r($auth->isLogin());
//        print_r($auth->getUserinfo());
//        print_r($auth->getUser());
//        exit();
        $this->config = get_addon_config('media');
        $this->assign("config",$this->config);

        $user = $auth->getUser();
        //插件用户
        $media_user = Db::name("media_user")->where(["user_id" => $user["id"]])->find();
        if (empty($media_user)) {
            Db::name("media_user")->insert(["user_id" => $user["id"], "createtime" => time(), "updatetime" => time()]);
        }
        $user["money"] = $media_user["money"];

        $this->assign("current_user", $user);
        $this->assign("auth_group", []);
    }

    public function init()
    {
        if ($this->auth->isLogin()) {
            $this->success("", "", $this->auth->getUser());
        } else {
            $this->error("请登录！");
        }
    }
    /**
     * 会员中心
     */
    public function index()
    {
        $this->view->assign('title', __('User center'));
        $data_list = Db::name("media_posts")->where(['channel' => 16, 'status' => 1])->order("id desc")->limit(5)->select();
        $this->assign("data_posts", $data_list);
        $data_list = Db::name("media_posts")->where(['channel' => 16, 'status' => 1, 'istop' => 1])->order("id desc")->limit(1)->select();
        $this->assign("data_notice", $data_list);

        $this->assign("mobilevalidate",1);
        return $this->fetch();
    }
    /*
     * 软文发布
     */
    public function release()
    {
        $this->assign("myprice", 2);
        $this->assign("channel", config("media_channel"));
        $this->assign("door", config("media_door"));
        $this->assign("area", config("media_area"));
        $this->assign("entry", config("media_entry"));
        $this->assign("allow", config("media_allow"));
        $this->assign("linktype", config("media_linktype"));
        $this->assign("newssource", config("media_newssource"));
        $this->assign("position", config("media_position"));
        //点击稿件过来投稿
        $draftid = input("param.draftid");
        $draft_info = ["content" => ""];
        if (isset($draftid)) {
            $draft_info = DraftModel::get($draftid);
        }
        //print_r($draft_info);exit();
        $this->assign("draft_info", $draft_info);

        if ($this->request->isPost()) {
            //
//            if(!$this->userModel->get(is_login())["level"]) {
//                $this->error("系统已禁止发稿，请尽快前往新平台升级账户！");
//            }
            $data = input("post.");
            $user = $this->auth->getUser();
//            print_r($user);exit();
            $data["media"] = trim($data["media"], ",");
            $data["meal"] = trim($data["meal"], ",");
            $data["wemedia"] = trim($data["wemedia"], ",");

            $draft["user_id"] = $user["id"];
//            include_once "../apps/common/lib/MyEnum.php";
            //$draft["type"] = draft_type::新闻稿;
            $draft["title"] = $data["title"];
            $draft["fromurl"] = $data["fromurl"];
            $draft["content"] = preg_replace('/[\x{10000}-\x{10FFFF}]/u', '', $data["content"]);
            //关键词过滤
            foreach (explode(',', str_replace("，", ",", $this->config["filter"])) as $f => $fv) {
                $draft["content"] = str_replace($fv, "***", $draft["content"]);
            }
            $draft["fileurl"] = $data["fileurl"];
            $draft["createtime"] = time();
            $draft["updatetime"] = time();
            $res = Db::name("media_draft")->insertGetId($draft);

            if ($res) {
                $data["user_id"] = $user["id"];
                $media_user = Db::name("media_user")->where(["user_id" => $user["id"]])->find();
                if (empty($media_user)) $this->error("账户信息异常！");
                $money = 0;
//                include "../apps/common/lib/BoxApi.php";
                if (!empty($data["meal"])) {
                    //判断余额
                    $cm = 0;
                    foreach (explode(",", $data["meal"]) as $k => $v) {
                        if (!empty($v)) {
                            //套餐价格
                            $meal = Db::name("media_meal")->where(["id" => $v])->find();
                            if (!empty($meal)) {
                                $cm = $cm + $meal["money"];
                            }
                        }
                    }
                    if ($media_user["money"] < $cm) {
                        $this->error("余额不足，请充值后再提交！");
                    }
                    //套餐消费
                    foreach (explode(",", $data["meal"]) as $k => $v) {
                        if (!empty($v)) {
                            //套餐价格
                            $meal = Db::name("media_meal")->where(["id" => $v])->find();
                            $ins_data["ordersn"] = date("YmdHis") . rand(10000, 99999);
                            $ins_data["user_id"] = $media_user["user_id"];
                            $ins_data["draft_id"] = $res;
                            $ins_data["meal"] = $v;
                            //获取套餐价格
                            $ins_data["money"] = $meal["money"];
                            $ins_data["createtime"] = time();
                            $ins_data["updatetime"] = time();
                            //
                            Db::name("media_goods_meal")->insertGetId($ins_data);
                            //累加扣款金额
                            $money = $money + $ins_data["money"];
                        }
                    }
                }
                if (!empty($data["media"])) {
                    //判断余额
                    $cm = 0;
                    foreach (explode(",", $data["media"]) as $k => $v) {
                        if (!empty($v)) {
                            $cm = $cm + GetUserGroupPrice("media", $v,$media_user["user_id"]);
                        }
                    }
                    if ($media_user["money"] < $cm) {
                        $this->error("余额不足，请充值后再提交！");
                    }
                    //自选媒体消费
                    foreach (explode(",", $data["media"]) as $k => $v) {
                        if (!empty($v)) {
                            $ins_data["ordersn"] = date("YmdHis") . rand(10000, 99999);
                            $ins_data["user_id"] = $media_user["user_id"];
                            $ins_data["draft_id"] = $res;
                            $ins_data["media"] = $v;
                            //获取媒体价格
                            $ins_data["money"] = GetUserGroupPrice("media", $v,$media_user["user_id"]);
                            //获取媒介价格
//                            $mms = new MediumMediaModel();
//                            $mm = $mms->where(["media" => $v, "status" => 1])->find();
//                            if ($mm) {
//                                $data["distribute"] = $mm["uid"];
//                                $data["distributemoney"] = $mm["money"];
//                            }
                            //获取媒介盒子id
                            $media = Db::name("media_media")->where(["id" => $v])->find();
                            $ins_data["boxid"] = $media["boxid"];
                            $ins_data["createtime"] = time();
                            $ins_data["updatetime"] = time();
                            //
                            Db::name("media_goods")->insertGetId($ins_data);
//                            if ($data["boxid"] && $media["memberprice4"] > $media["boxcostprice"]) {
//                                //推送订单到媒介盒子
//                                $map['content'] = "<a href='https://www.apinxuan.com/index.php/home/index/goodsdetail/t/goods/id/" . $gm->id . ".html'>稿件链接</a>";
//                                $map['media_id'] = $data["boxid"];
//                                $map['title'] = urlencode($data["title"]);            //稿件标题必须单独使用urlencode();
//                                $map['xwy_ordernum'] = 'apinxuan_' . $data["ordersn"];
////                                $ts = my_curl($url_create_order, $map);
//                                //
////                                file_put_contents("tuisong-" . date("Y-m-d", time()) . ".txt", date("Y-m-d H:i:s", time()) . "\t" . $ts . "\r\n", FILE_APPEND);
//                            }
                            //累加扣款金额
                            $money = $money + $ins_data["money"];
                        }
                    }
                }
                if (!empty($data["wemedia"])) {
                    //判断余额
                    $cm = 0;
                    foreach (explode(",", $data["wemedia"]) as $k => $v) {
                        if (!empty($v)) {
                            $cm = $cm + GetUserGroupPrice("wemedia", $v,$media_user["user_id"]);
                        }
                    }
                    if ($media_user["money"] < $cm) {
                        $this->error("余额不足，请充值后再提交！");
                    }
                    //自媒体消费
                    foreach (explode(",", $data["wemedia"]) as $k => $v) {
                        if (!empty($v)) {
                            $ins_data["ordersn"] = date("YmdHis") . rand(10000, 99999);
                            $ins_data["user_id"] = $media_user["user_id"];
                            $ins_data["draft_id"] = $res;
                            $ins_data["wemedia"] = $v;
                            //获取媒体价格
                            $ins_data["money"] = GetUserGroupPrice("wemedia", $v,$media_user["user_id"]);
                            //获取媒介价格
//                            $mms = new MediumMediaModel();
//                            $mm = $mms->where(["media" => $v, "status" => 1])->find();
//                            if ($mm) {
//                                $data["distribute"] = $mm["uid"];
//                                $data["distributemoney"] = $mm["money"];
//                            }
                            //获取媒介盒子id
                            $wemedia = Db::name("media_wemedia")->where(["id" => $v])->find();
                            $ins_data["boxid"] = $wemedia["boxid"];
                            $ins_data["createtime"] = time();
                            $ins_data["updatetime"] = time();
                            //
                            Db::name("media_goods_wemedia")->insertGetId($ins_data);
//                            if ($data["boxid"] && $wemedia["memberprice4"] > $wemedia["boxcostprice"]) {
//                                //推送订单到媒介盒子
//                                $map['content'] = "<a href='https://www.apinxuan.com/index.php/home/index/goodsdetail/t/goodswemedia/id/" . $gwm->id . ".html'>稿件链接</a>";
//                                $map['wemedia_id'] = $data["boxid"];
//                                $map['title'] = urlencode($data["title"]);            //稿件标题必须单独使用urlencode();
//                                $map['xwy_ordernum'] = 'apinxuan_wemedia_' . $data["ordersn"];
//                                $map["selling_price"] = 999999;
//                                $map["article_remark"] = "";
////                                my_curl($url_create_weorder, $map);
//                            }
                            //累加扣款金额
                            $money = $money + $ins_data["money"];
                        }
                    }
                }
                //扣除用户金额
                $u["money"] = $media_user["money"] - $money;
                //
                Db::name("media_user")->where(["user_id" => $media_user["user_id"]])->update($u);
//                $this->userModel->editData($u, $user["uid"], "uid");
                $this->success("亲，您的稿件已成功提交，小编正为你拼命出稿，请耐心等待！", addon_url('media/member/releaselist'));
//                echo "﻿{\"code\":1,\"msg\":\"亲，您的稿件已成功提交，小编正为你拼命出稿，请耐心等待！\",\"data\":\"\",\"url\":\"\/index.php\/member\/goods\/releaselist.html\",\"wait\":3}";exit();
            } else
                $this->success("遗憾，提交失败！", addon_url('media/member/release'));
        }
        return $this->fetch();
    }
    /*
     * 发布记录
     */
    public function releaselist()
    {
        // 搜索
        $map["g.user_id"] = $this->auth->getUser()["id"];
        $keyword = input('param.keyword');
        if ($keyword) {
            $map["title"] = ['like', '%' . $keyword . '%'];
        }
        if (!empty(input("param.start")) && !empty(input("param.end"))) {
            $map["g.create_time"] = array(array("egt", strtotime(input("param.start"))), array("elt", strtotime(input("param.end")." 23:59:59")));
        } else {
            if (!empty(input("param.start"))) $map["g.create_time"] = ["egt", strtotime(input("param.start"))];
            if (!empty(input("param.end"))) $map["g.create_time"] = ["elt", strtotime(input("param.end"))];
        }
        if (!empty(input("param.status")) || input("param.status") == "0") $map["g.status"] = input("param.status");
        $data_list = Db::name("media_goods")->alias("g")
            ->join("media_draft d", "g.draft_id=d.id","LEFT")
            ->field("g.*,d.title")
            ->where($map)
            ->order('id desc')
            ->paginate(20, false, [
            'query' => ['start' => input("param.start"), 'end' => input("param.end"),
                'status' => input("param.status"), 'keyword' => input("param.keyword")]
        ])
            ->each(function ($item,$key){
                $item['media_name'] = Db::name("media_media")->where(["id" => $item['media']])->find()["name"];
                return $item;
            });

        //print_r($data_list);exit();
        $this->assign("data_list", $data_list);

        //代写记录
        $map1=["user_id"=>$this->auth->getUser()["id"]];
        $data_list = Db::name("media_goods_meal")->where($map1)->field(true)->order('id desc')->paginate(20);
        //遍历posts遍历的数据
        foreach ($data_list as &$row) {
//            $row['draft_title'] = DraftModel::get($row['draft'])["title"];//获得term名称
//            $row['meal_name'] = MealModel::get($row['meal'])["name"];//获得term名称
        }
        //print_r($data_list);exit();
        $this->assign("data_list2",$data_list);
        return $this->fetch();
    }

    public function releasewemedialist()
    {
        // 搜索
        $map["g.user_id"] = $this->auth->getUser()["id"];
        $keyword = input('param.keyword');
        if ($keyword) {
            $map["title"] = ['like', '%' . $keyword . '%'];
        }
        if (!empty(input("param.start")) && !empty(input("param.end"))) {
            $map["g.create_time"] = array(array("egt", strtotime(input("param.start"))), array("elt", strtotime(input("param.end"))));
        } else {
            if (!empty(input("param.start"))) $map["g.create_time"] = ["egt", strtotime(input("param.start"))];
            if (!empty(input("param.end"))) $map["g.create_time"] = ["elt", strtotime(input("param.end"))];
        }
        if (!empty(input("param.status")) || input("param.status") == "0") $map["g.status"] = input("param.status");
        $data_list = Db::name("media_goods_wemedia")->alias("g")->join("media_draft d", "g.draft_id=d.id", "LEFT")->field("g.*,d.title")->where($map)->order('id desc')->paginate(20, false, [
            'query' => ['start' => input("param.start"), 'end' => input("param.end"),
                'status' => input("param.status"), 'keyword' => input("param.keyword")]
        ])
            ->each(function ($item, $key) {
                $mm = Db::name("media_wemedia")->where(["id" => $item['wemedia']])->find();
                $item['wemedia_name'] = $mm["name"];
                $item["platform"] = empty($mm["platform"]) ? "" : $this->config["wemedia_platform"][$mm["platform"]];
                return $item;
            });
        $this->assign("data_list", $data_list);

        //print_r($data_list);exit();
        return $this->fetch();
    }

    public function releasemeallist()
    {
        $map["g.user_id"] = $this->auth->getUser()["id"];
        $data_list = Db::name("media_goods_wemedia")->alias("g")
            ->join("media_draft d", "g.draft_id=d.id", "LEFT")->field("g.*,d.title")
            ->where($map)
            ->order('id desc')
            ->paginate(20)
            ->each(function ($item, $key) {
                $mm = Db::name("media_meal")->where(["id" => $item['meal']])->find();
                $item['meal_name'] = $mm["name"];
                //
                $html = "";
                $arr = explode("\r\n", trim($item["url"]));
                foreach ($arr as $k => $val) {
                    $arr2 = explode("：", $val);
                    if (count($arr2) > 1) {
                        $html .= $arr2[0] . "：<a href=\"" . $arr2[1] . "\" target=\"_blank\">" . $arr2[1] . "</a><br/>";
                    }
                }
                $item["url"] = $html;
                return $item;
            });

        //print_r($data_list);exit();
        $this->assign("data_list", $data_list);
        $this->assign("status", $this->config["goods_status"]);
        return $this->fetch();
    }

    /*
     * 查看草稿
     */
    public function drafttemp()
    {

    }

    public function pay()
    {


        return $this->fetch();
    }

    public function payhistory()
    {
        $map=["user_id"=>$this->auth->getUser()["id"]];
        $type = input('param.type');
        if ($type) {
            $map["type"]=$type;
        }
        $data_list = Db::name("media_pay")->where($map)->field(true)->order('id desc')->paginate(20);
        //print_r($data_list);exit();
        $this->assign("data_list",$data_list);
        return $this->fetch();
    }

    public function userinfo()
    {
//        $this->assign("user",UserModel::get(session("user_login_auth")["uid"]));
        if ($this->request->isPost()) {
            $data = input("post.");
            Db::name("user")->where(["id" => $this->auth->getUser()["id"]])->update($data);
            $this->success("信息更新成功！", addon_url("media/member/userinfo"));
        }
        return $this->fetch();
    }
    /*
     *
     */
    public function ajax()
    {
//        //
//        if(!empty(input("param.filter"))) {
//            $fr = "";
//            foreach (explode(',', str_replace("，", ",", config("web_site_filter"))) as $f => $fv) {
//                if (stripos(input("post.content"), $fv)) $fr .= $fv . ",";
//            }
//            if (!empty($fr)) {
//                $this->error($fr);
//            } else $this->success("已通过！");
//        }
        $user=$this->auth->getUser();
        $data = input("param.");
//        print_r($data);
        switch ($data["t"]) {
            case "filter":
                {
                    $fr = "";
                    foreach (explode(',', str_replace("，", ",", config("web_site_filter"))) as $f => $fv) {
                        if (stripos(input("post.content"), $fv)) $fr .= $fv . ",";
                    }
                    if (!empty($fr)) {
                        $this->error($fr);
                    } else $this->success("已通过！");
                }
                break;
            case "draft":
                {
                    //初始化草稿内容
                    $top = Db::name("media_draft_temp")->where(["type" => $data["type"], "user_id" => $user["id"]])->find();
                    if (!empty($top)) $this->success("", "", ["title" => $top["title"], "fromurl" => $top["fromurl"], "content" => $top["content"], "fileurl" => $top["fileurl"]]);
                    else $this->error("");
                }
                break;
            case "savedraft":
                {
                    //保存草稿
                    $top = Db::name("media_draft_temp")->where(["type" => $data["type"], "user_id" => $user["id"]])->find();
                    $data["title"] = urldecode($data["title"]);
                    $data["content"] = urldecode($data["content"]);
                    $data["content"] = preg_replace('/[\x{10000}-\x{10FFFF}]/u', '', $data["content"]);
//                    print_r($data);
//                    exit();
                    if (!empty($data["title"]) && (!empty($data["content"]) || !empty($data["fileurl"]))) {
                        $ins_data = [
                            "user_id" => $user["id"],
                            "type" => $data["type"],
                            "title" => $data["title"],
                            "fromurl" => $data["fromurl"],
                            "content" => $data["content"],
                            "fileurl" => $data["fileurl"]
                        ];
                        if (empty($top)) {
                            $result = Db::name("media_draft_temp")->insert($ins_data);
                        } else {
                            $result = Db::name("media_draft_temp")->where(["id" => $top["id"]])->update($ins_data);
                        }
                        if ($result) $this->success("ok");
                    }
                }
                break;
            case "media":
                {
//                    print_r("ssssssssss");exit();
                    //加载媒体
                    $default_sel = [0 => ""];
                    $channel_list = $default_sel + $this->config["media_channel"];
                    $door_list = $default_sel + $this->config["media_door"];
                    $area_list = $default_sel + $this->config["media_area"];
                    $entry_list = $default_sel + $this->config["media_entry"];
                    $allow_list = $default_sel + $this->config["media_allow"];
                    $linktype_list = $default_sel + $this->config["media_linktype"];
                    $newssource_list = $this->config["media_newssource"];
                    $position_list = $default_sel + $this->config["media_position"];
                    $publicationtime_list=$default_sel+[1=>"一小时内",
                            2=>"两小时内",
                            12=>"半天",
                            24=>"当天",
                            48=>"隔天",
                            49=>"2天以上"];

                    $sort="number1 asc";
                    switch ($data["sort"]) {
                        case "10":
                            $sort = "memberprice4 asc";
                            break;
                        case "11":
                            $sort = "memberprice4 desc";
                            break;
                        case "20":
                            $sort = "publishrate asc";
                            break;
                        case "21":
                            $sort = "publishrate desc";
                            break;
                        case "30":
                            $sort = "pcweight asc";
                            break;
                        case "31":
                            $sort = "pcweight desc";
                            break;
                        case "40":
                            $sort = "mweight asc";
                            break;
                        case "41":
                            $sort = "mweight desc";
                            break;
                        case "50":
                            $sort = "publicationtime asc";
                            break;
                        case "51":
                            $sort = "publicationtime desc";
                            break;
                    }
                    if(empty($data["cfmt"]) && $data["cfmt"]!=0) {
                        //所有媒体
                        $map["status"] = 1;
                        $map["updown"] = 1;
                        if (!empty($data["channel"])) $map["channel"] = $data["channel"];
                        if (!empty($data["door"])) $map["door"] = $data["door"];
                        if (!empty($data["area"])) $map["area"] = $data["area"];
                        if (!empty($data["newssource"]) || $data["newssource"]=="0") $map["newssource"] = $data["newssource"];
                        if (!empty($data["position"])) $map["position"] = $data["position"];
                        if (!empty($data["linktype"])) $map["linktype"] = $data["linktype"];
                        if (!empty($data["weekfa"]) || $data["weekfa"]=="0") $map["weekfa"] = $data["weekfa"];
                        if (!empty($data["includecondition"]) || $data["includecondition"]=="0") $map["includecondition"] = $data["includecondition"];
                        if (!empty($data["keyword"])) $map["name"] = array('like', '%' . urldecode($data["keyword"]) . '%');
                        print_r($channel_list);exit();
                        $d = Db::name("media_media")->where($map)->field(true)->order($sort)->limit(($data["page"]-1)*20,20)->select();
                        //遍历posts遍历的数据
                        foreach ($d as &$row) {
                            try {
                                $row['channel_name'] = $channel_list[$row['channel']];
                                $row['door_name'] = $door_list[$row['door']];
                                $row['area_name'] = $area_list[$row['area']];
                                $row['entry_name'] = $entry_list[$row['entry']];
                                $row['allow_name'] = $allow_list[$row['allow']];
                                $row['linktype_name'] = $linktype_list[$row['linktype']];
                                $row['newssource_name'] = $newssource_list[$row['newssource']];
                                $row['publicationtime_name'] = $publicationtime_list[$row['publicationtime']];
                                $row['position_name'] = $position_list[$row['position']];
                            }
                            catch (Exception $e){

                            }
//                if (!empty($data["myprice"])) {
//                    $row["myprice"] = GetUserGroupPrice(session("user_login_auth")["uid"], $row["id"]);
//                }
                        }

                    }
                    else {
                        if ($data["cfmt"] < 4) {
                            //媒体及收藏媒体
                            $map["status"] = 1;
                            $map["updown"] = 1;
                            if (!empty($data["channel"])) $map["channel"] = $data["channel"];
                            if (!empty($data["door"])) $map["door"] = $data["door"];
                            if (!empty($data["area"])) $map["area"] = $data["area"];
                            if (!empty($data["newssource"]) || $data["newssource"]=="0") $map["newssource"] = $data["newssource"];
                            if (!empty($data["position"])) $map["position"] = $data["position"];
                            if (!empty($data["linktype"])) $map["linktype"] = $data["linktype"];
                            if (!empty($data["weekfa"]) || $data["weekfa"]=="0") $map["weekfa"] = $data["weekfa"];
                            if (!empty($data["includecondition"]) || $data["includecondition"]=="0") $map["includecondition"] = $data["includecondition"];
                            if (!empty($data["keyword"])) $map["name"] = array('like', '%' . urldecode($data["keyword"]) . '%');
                            //print_r($channel_list);exit();
                            switch ($data["cfmt"]) {
                                case 0:
                                    //$map["recom"] = recom::全局推荐;
                                    {
                                        $media = $this->pushModel->where(["uid" => 1])->find();
                                        if ($media) {
                                            $map["id"] = ["in", $media["media"]];
                                        } else $map["id"] = ["in", "0"];
                                        $d = Db::name("media_media")->where($map)->field(true)->order($sort)->limit(($data["page"]-1)*20,20)->select();
                                    }
                                    break;
                                case 1:
//                        if (count($map) == 2 && empty($data["loadmore"])) {
//                            //所有媒体直接读取缓存js
//                            echo file_get_contents("./uploads/file/media.js");
//                            exit();
//                        }
                                    $d = Db::name("media_media")->where($map)->field(true)->order($sort)->limit(($data["page"]-1)*20,20)->select();

                                    break;
                                case 2:
                                    $d = Db::name("media_media")->alias('m')->join('favorite f', 'f.uid=' . session("user_login_auth")["uid"] . ' and f.objectid = m.id and f.type=' . favorite_type::媒体)->where($map)->field("m.*")->order('number1 asc')->select();
                                    break;
                                case 3:
                                    {
                                        $media = $this->pushModel->where(["uid" => session("user_login_auth")["uid"]])->find();
                                        if ($media) {
                                            $map["id"] = ["in", $media["media"]];
                                        } else $map["id"] = ["in", "0"];
                                        $d = Db::name("media_media")->where($map)->field(true)->order($sort)->select();
                                    }
                                    break;
                            }
                            //遍历posts遍历的数据
                            foreach ($d as &$row) {
                                try {
                                    $row['channel_name'] = $channel_list[$row['channel']];
                                    $row['door_name'] = $door_list[$row['door']];
                                    $row['area_name'] = $area_list[$row['area']];
                                    $row['entry_name'] = $entry_list[$row['entry']];
                                    $row['allow_name'] = $allow_list[$row['allow']];
                                    $row['linktype_name'] = $linktype_list[$row['linktype']];
                                    $row['newssource_name'] = $newssource_list[$row['newssource']];
                                    $row['publicationtime_name'] = $publicationtime_list[$row['publicationtime']];
                                    $row['position_name'] = $position_list[$row['position']];
//                    if (!empty($data["myprice"])) {
//                        $row["myprice"] = GetUserGroupPrice(session("user_login_auth")["uid"], $row["id"]);
//                    }
                                }
                                catch (Exception $e){

                                }
                            }
                        } else {
                            //套餐及收藏套餐
                            $map = ["type" => meal_type::通用套餐];
                            switch ($data["cfmt"]) {
                                case 4:
                                    {
                                        $d = $this->mealModel->where($map)->field(true)->order('id desc')->select();
                                        //序列化media
                                        foreach ($d as &$row) {
                                            $m=[];
                                            if ($row["media"]) {
                                                $m["id"] = ["in", $row["media"]];
                                            } else $m["id"] = ["in", "0"];
                                            $top = Db::name("media_media")->where($m)->select();
                                            $html = "";
                                            foreach ($top as &$row2) {
                                                $html .= "<a href=\"/media/".$row2["id"].".html\" target=\"_blank\">" . $row2["name"] . "</a> ";
                                            }
                                            $row["medias"] = $html;
                                        }
                                    }
                                    break;
                                case 5:
                                    $d = $this->mealModel->alias('m')->join('favorite f', 'f.uid=' . session("user_login_auth")["uid"] . ' and f.objectid = m.id and f.type=' . favorite_type::套餐)->where(["m.type" => meal_type::通用套餐])->field("m.*")->order('id desc')->select();
                                    break;
                            }
                        }
                    }
                    $result = array(
                        'code' => 1,
                        'msg' => '',
                        'count' => count($d),
                        'list' => $d
                    );
                    //输出json
                    echo json_encode($result);
                }
                break;

        }
    }


}