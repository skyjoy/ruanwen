<?php

namespace addons\media\controller;

use think\addons\Controller;
use think\Db;
use think\Exception;


class Index extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->config = get_addon_config('media');
        $this->assign("config",$this->config);
        //
        $data_result = Db::name("media_links")->where(["status" => 1, "type" => 2, "rating" => 3])->field(true)->order("sort desc")->limit(1)->select();
        foreach ($data_result as &$row) {
            //图片附件
//            $result = Attachment::get($row["image"]);
//            if (!empty($result["path"])) $row["image"] = $result["path"];
        }
        if (empty($data_result))
            $this->assign("secbanner", "");
        else
            $this->assign("secbanner", $data_result[0]["image"]);
    }

    public function index()
    {
//        $this->error("当前插件暂无前台页面");
//        $data_list = Db::name("media_posts")->where('status=1 AND img<>0')->order("istop desc,id desc")->limit(4)->select();
//        foreach ($data_list as &$row) {
//            //图片附件
//            $result = Attachment::get($row["img"]);
//            if (!empty($result["path"])) $row["img"] = $result["path"];
//        }
//        $this->assign("data_postspic",$data_list);

        $cache = cache('data_link_rating_1');
        if (!$cache || empty($cache)) {
            $data_list = Db::name("media_links")->where(["status" => 1, "type" => 2, "rating" => 1])->field(true)->order("sort desc")->limit(10)->select();
            foreach ($data_list as &$row) {
                //图片附件
//                $result = Attachment::get($row["image"]);
//                if (!empty($result["path"])) $row["image"] = $result["path"];
            }
            cache('data_link_rating_1', $data_list, 3600);
        }
        $this->assign("data_link_rating_1", cache("data_link_rating_1"));
//        //手机站banner
//        $cache = cache('data_link_rating_1_1');
//        if (!$cache || empty($cache)) {
//            $data_list = Db::name("media_links")->where(["status" => 1, "type" => 2, "rating" => link_rating::手机站banner])->field(true)->order("sort desc")->limit(10)->select();
//            foreach ($data_list as &$row) {
//                //图片附件
//                $result = Attachment::get($row["image"]);
//                if (!empty($result["path"])) $row["image"] = $result["path"];
//            }
//            cache('data_link_rating_1_1', $data_list, 3600);
//        }
//        $this->assign("data_link_rating_1_1", cache("data_link_rating_1_1"));
//        ///////////媒体/////////////////
        $cache = cache('data_list_1');
        if (!$cache || empty($cache)) {
            $data_list = Db::name("media_media")->where(["status" => 1, "channel" => 1, "recom" => 1])->field(true)->limit(18)->select();
            foreach ($data_list as &$row) {
                //图片附件
//                $result = Attachment::get($row["photo"]);
//                if (!empty($result["path"])) $row["photo"] = $result["path"];
            }
            cache('data_list_1', $data_list, 3600);
        }
        $this->assign("data_list_1", cache("data_list_1"));
        $cache = cache('data_list_6');
        if (!$cache || empty($cache)) {
            $data_list = Db::name("media_media")->where(["status" => 1, "channel" => 6, "recom" => 1])->field(true)->limit(18)->select();
            foreach ($data_list as &$row) {
                //图片附件
//                $result = Attachment::get($row["photo"]);
//                if (!empty($result["path"])) $row["photo"] = $result["path"];
            }
            cache('data_list_6', $data_list, 3600);
        }
        $this->assign("data_list_6", cache("data_list_6"));
        $cache = cache('data_list_4');
        if (!$cache || empty($cache)) {
            $data_list = Db::name("media_media")->where(["status" => 1, "channel" => 4, "recom" => 1])->field(true)->limit(18)->select();
            foreach ($data_list as &$row) {
                //图片附件
//                $result = Attachment::get($row["photo"]);
//                if (!empty($result["path"])) $row["photo"] = $result["path"];
            }
            cache('data_list_4', $data_list, 3600);
        }
        $this->assign("data_list_4", cache("data_list_4"));
        $cache = cache('data_list_7');
        if (!$cache || empty($cache)) {
            $data_list = Db::name("media_media")->where(["status" => 1, "channel" => 7, "recom" => 1])->field(true)->limit(18)->select();
            foreach ($data_list as &$row) {
                //图片附件
//                $result = Attachment::get($row["photo"]);
//                if (!empty($result["path"])) $row["photo"] = $result["path"];
            }
            cache('data_list_7', $data_list, 3600);
        }
        $this->assign("data_list_7", cache("data_list_7"));
        $cache = cache('data_list_13');
        if (!$cache || empty($cache)) {
            $data_list = Db::name("media_media")->where(["status" => 1, "channel" => 13, "recom" => 1])->field(true)->limit(18)->select();
            foreach ($data_list as &$row) {
                //图片附件
//                $result = Attachment::get($row["photo"]);
//                if (!empty($result["path"])) $row["photo"] = $result["path"];
            }
            cache('data_list_13', $data_list, 3600);
        }
        $this->assign("data_list_13", cache("data_list_13"));
        $cache = cache('data_list_9');
        if (!$cache || empty($cache)) {
            $data_list = Db::name("media_media")->where(["status" => 1, "channel" => 9, "recom" => 1])->field(true)->limit(18)->select();
            foreach ($data_list as &$row) {
                //图片附件
//                $result = Attachment::get($row["photo"]);
//                if (!empty($result["path"])) $row["photo"] = $result["path"];
            }
            cache('data_list_9', $data_list, 3600);
        }
        $this->assign("data_list_9", cache("data_list_9"));
        $cache = cache('data_list_16');
        if (!$cache || empty($cache)) {
            $data_list = Db::name("media_media")->where(["status" => 1, "channel" => 16, "recom" => 1])->field(true)->limit(18)->select();
            foreach ($data_list as &$row) {
                //图片附件
//                $result = Attachment::get($row["photo"]);
//                if (!empty($result["path"])) $row["photo"] = $result["path"];
            }
            cache('data_list_16', $data_list, 3600);
        }
        $this->assign("data_list_16", cache("data_list_16"));
//        /////////////首页推荐///////////////
        $cache = cache('data_list_position_1');
        if (!$cache || empty($cache)) {
            $data_list = Db::name("media_media")->where(["status" => 1, "position" => 2, "channel" => 1])->field(true)->limit(18)->select();
            foreach ($data_list as &$row) {
                //图片附件
//                $result = Attachment::get($row["photo"]);
//                if (!empty($result["path"])) $row["photo"] = $result["path"];
            }
            cache('data_list_position_1', $data_list, 3600);
        }
        $this->assign("data_list_position_1", cache("data_list_position_1"));
        $cache = cache('data_list_position_6');
        if (!$cache || empty($cache)) {
            $data_list = Db::name("media_media")->where(["status" => 1, "position" => 2, "channel" => 6])->field(true)->limit(18)->select();
            foreach ($data_list as &$row) {
                //图片附件
//                $result = Attachment::get($row["photo"]);
//                if (!empty($result["path"])) $row["photo"] = $result["path"];
            }
            cache('data_list_position_6', $data_list, 3600);
        }
        $this->assign("data_list_position_6", cache("data_list_position_6"));
        $cache = cache('data_list_position_4');
        if (!$cache || empty($cache)) {
            $data_list = Db::name("media_media")->where(["status" => 1, "position" => 2, "channel" => 4])->field(true)->limit(18)->select();
            foreach ($data_list as &$row) {
                //图片附件
//                $result = Attachment::get($row["photo"]);
//                if (!empty($result["path"])) $row["photo"] = $result["path"];
            }
            cache('data_list_position_4', $data_list, 3600);
        }
        $this->assign("data_list_position_4", cache("data_list_position_4"));
        $cache = cache('data_list_position_7');
        if (!$cache || empty($cache)) {
            $data_list = Db::name("media_media")->where(["status" => 1, "position" => 2, "channel" => 7])->field(true)->limit(18)->select();
            foreach ($data_list as &$row) {
                //图片附件
//                $result = Attachment::get($row["photo"]);
//                if (!empty($result["path"])) $row["photo"] = $result["path"];
            }
            cache('data_list_position_7', $data_list, 3600);
        }
        $this->assign("data_list_position_7", cache("data_list_position_7"));
        $cache = cache('data_list_position_13');
        if (!$cache || empty($cache)) {
            $data_list = Db::name("media_media")->where(["status" => 1, "position" => 2, "channel" => 13])->field(true)->limit(18)->select();
            foreach ($data_list as &$row) {
                //图片附件
//                $result = Attachment::get($row["photo"]);
//                if (!empty($result["path"])) $row["photo"] = $result["path"];
            }
            cache('data_list_position_13', $data_list, 3600);
        }
        $this->assign("data_list_position_13", cache("data_list_position_13"));
        $cache = cache('data_list_position_9');
        if (!$cache || empty($cache)) {
            $data_list = Db::name("media_media")->where(["status" => 1, "position" => 2, "channel" => 9])->field(true)->limit(18)->select();
            foreach ($data_list as &$row) {
                //图片附件
//                $result = Attachment::get($row["photo"]);
//                if (!empty($result["path"])) $row["photo"] = $result["path"];
            }
            cache('data_list_position_9', $data_list, 3600);
        }
        $this->assign("data_list_position_9", cache("data_list_position_9"));
        $data_list = Db::name("media_media")->where(["status" => 1, "position" => 2, "channel" => 16])->field(true)->limit(18)->select();
        foreach ($data_list as &$row) {
            //图片附件
//            $result = Attachment::get($row["photo"]);
//            if (!empty($result["path"])) $row["photo"] = $result["path"];
        }
        $this->assign("data_list_position_16", $data_list);

        //        //套餐
        $data_list = Db::name("media_meal")->where(["type" => 1])->field(true)->order("number1 asc")->limit(5)->select();
        foreach ($data_list as &$row) {
            //图片附件
//            $result = Attachment::get($row["photo"]);
//            if (!empty($result["path"])) $row["photo"] = $result["path"];
        }
        $this->assign("data_meal", $data_list);

        //案例
        $cache = cache('data_anli');
        if (!$cache || empty($cache)) {
            $data_list = [];//Db::name("media_posts")->alias("p")->join("term_relationships tt", "p.id=tt.object_id")->join("terms t", "t.term_id=tt.term_id")->field("p.*,t.term_id,t.slug")->where(['t.slug' => "anli", 'p.status' => 1])->order("p.id desc")->limit(12)->select();
            foreach ($data_list as &$row) {
                //图片附件
//                $result = Attachment::get($row["img"]);
//                if (!empty($result["path"])) $row["img"] = $result["path"];
            }
            cache('data_anli', $data_list, 3600);
        }
        $this->assign("data_anli", cache("data_anli"));

        //品牌 合作伙伴
        $cache = cache('data_link_rating_2');
        if (!$cache || empty($cache)) {
            $data_list = Db::name("media_links")->where(["status" => 1, "type" => 2, "rating" => 2])->field(true)->order("sort desc")->limit(10)->select();
            foreach ($data_list as &$row) {
                //图片附件
//                $result = Attachment::get($row["image"]);
//                if (!empty($result["path"])) $row["image"] = $result["path"];
            }
            cache('data_link_rating_2', $data_list, 3600);
        }
        $this->assign("data_link_rating_2", cache("data_link_rating_2"));
        $cache = cache('data_link_rating_3');
        if (!$cache || empty($cache)) {
            $data_list = Db::name("media_links")->where(["status" => 1, "type" => 2, "rating" => 3])->field(true)->order("sort desc")->limit(10)->select();
            foreach ($data_list as &$row) {
                //图片附件
//                $result = Attachment::get($row["image"]);
//                if (!empty($result["path"])) $row["image"] = $result["path"];
            }
            cache('data_link_rating_3', $data_list, 3600);
        }
        $this->assign("data_link_rating_3", cache("data_link_rating_3"));
        //近期交易
        $data_list = Db::name("media_goods")->field(true)->order("id desc")->limit(20)->select();
        foreach ($data_list as &$row) {
            $row["username"] =Db::name("user")->where(["id"=>$row["user_id"]])->find()["username"];
        }
        $this->assign("data_goods", $data_list);
        $data_list = Db::name("media_goods_meal")->field(true)->order("id desc")->limit(10)->select();
        foreach ($data_list as &$row) {
            $row["username"] = get_user_info($row["uid"])["username"];
        }
        $this->assign("data_goodsmeal", $data_list);
        $cache = cache('data_pay');
        if (!$cache || empty($cache)) {
            $data_list = Db::name("media_pay")->where(["status" => 2, "type" => array("ELT", 3)])->field(true)->order("id desc")->limit(30)->select();
            foreach ($data_list as &$row) {
                $row["username"] = get_user_info($row["uid"])["username"];
            }
            cache('data_pay', $data_list, 3600);
        }
        $this->assign("data_pay", cache("data_pay"));
        $cache = cache('data_media');
        if (!$cache || empty($cache)) {
            $data_list = Db::name("media_media")->where(["status" => 1])->field(true)->order("id desc")->limit(30)->select();
            cache('data_media', $data_list, 3600);
        }
        $this->assign("data_media", cache("data_media"));
        //资讯中心
        $cache = cache('data_posts1_r');
        if (!$cache || empty($cache)) {
            $data_list = [];//Db::name("media_posts")->alias("p")->join("term_relationships t", "p.id=t.object_id")->field("p.*,t.term_id")->where(['t.term_id' => 1, 'p.recommended' => 1, 'p.status' => 1])->order("p.id desc")->limit(2)->select();
            foreach ($data_list as &$row) {
                //图片附件
//                $result = Attachment::get($row["img"]);
//                if (!empty($result["path"])) $row["img"] = $result["path"];
            }
            cache('data_posts1_r', $data_list, 3600);
        }
        $this->assign("data_posts1_r", cache("data_posts1_r"));
        $cache = cache('data_posts1');
        if (!$cache || empty($cache)) {
            $data_list = [];//Db::name("media_posts")->alias("p")->join("term_relationships t", "p.id=t.object_id")->field("p.*,t.term_id")->where(['t.term_id' => 1, 'p.status' => 1])->order("p.id desc")->limit(3)->select();
            cache('data_posts1', $data_list, 3600);
        }
        $this->assign("data_posts1", cache("data_posts1"));
        $cache = cache('data_posts15_r');
        if (!$cache || empty($cache)) {
            $data_list = [];//Db::name("media_posts")->alias("p")->join("term_relationships t", "p.id=t.object_id")->field("p.*,t.term_id")->where(['t.term_id' => 15, 'p.recommended' => 1, 'p.status' => 1])->order("p.id desc")->limit(2)->select();
            foreach ($data_list as &$row) {
                //图片附件
//                $result = Attachment::get($row["img"]);
//                if (!empty($result["path"])) $row["img"] = $result["path"];
            }
            cache('data_posts15_r', $data_list, 3600);
        }
        $this->assign("data_posts15_r", cache("data_posts15_r"));
        $cache = cache('data_posts15');
        if (!$cache || empty($cache)) {
            $data_list = [];//Db::name("media_posts")->alias("p")->join("term_relationships t", "p.id=t.object_id")->field("p.*,t.term_id")->where(['t.term_id' => 15, 'p.status' => 1])->order("p.id desc")->limit(3)->select();
            cache('data_posts15', $data_list, 3600);
        }
        $this->assign("data_posts15", cache("data_posts15"));
        $cache = cache('data_posts16_r');
        if (!$cache || empty($cache)) {
            $data_list = [];//Db::name("media_posts")->alias("p")->join("term_relationships t", "p.id=t.object_id")->field("p.*,t.term_id")->where(['t.term_id' => 16, 'p.recommended' => 1, 'p.status' => 1])->order("p.id desc")->limit(2)->select();
            foreach ($data_list as &$row) {
                //图片附件
//                $result = Attachment::get($row["img"]);
//                if (!empty($result["path"])) $row["img"] = $result["path"];
            }
            cache('data_posts16_r', $data_list, 3600);
        }
        $this->assign("data_posts16_r", cache("data_posts16_r"));
        $cache = cache('data_posts16');
        if (!$cache || empty($cache)) {
            $data_list = [];//Db::name("media_posts")->alias("p")->join("term_relationships t", "p.id=t.object_id")->field("p.*,t.term_id")->where(['t.term_id' => 16, 'p.status' => 1])->order("p.id desc")->limit(3)->select();
            cache('data_posts16', $data_list, 3600);
        }
        $this->assign("data_posts16", cache("data_posts16"));
        $cache = cache('data_posts15_r');
        if (!$cache || empty($cache)) {
            $data_list = [];//Db::name("media_posts")->alias("p")->join("term_relationships t", "p.id=t.object_id")->field("p.*,t.term_id")->where(['t.term_id' => 15, 'p.recommended' => 1, 'p.status' => 1])->order("p.id desc")->limit(2)->select();
            foreach ($data_list as &$row) {
                //图片附件
//                $result = Attachment::get($row["img"]);
//                if (!empty($result["path"])) $row["img"] = $result["path"];
            }
            cache('data_posts15_r', $data_list, 3600);
        }
        $this->assign("data_posts15_r", cache("data_posts15_r"));
        $cache = cache('data_posts15');
        if (!$cache || empty($cache)) {
            $data_list = [];//Db::name("media_posts")->alias("p")->join("term_relationships t", "p.id=t.object_id")->field("p.*,t.term_id")->where(['t.term_id' => 15, 'p.status' => 1])->order("p.id desc")->limit(3)->select();
            cache('data_posts15', $data_list, 3600);
        }
        $this->assign("data_posts15", cache("data_posts15"));

        $this->assign("pay", config("pay_type"));
        $this->assign("channel", config("media_channel"));
        $this->assign("p_uid", input("param.ref", 0));//推广来源
        //微信资源
//        $data_list = $this->weixinModel->where(["status" => 1, "recom" => 1, "channel" => 37])->field(true)->limit(8)->select();
//        $this->assign("data_list_weixin_37", $data_list);
//        $data_list = $this->weixinModel->where(["status" => 1, "recom" => 1, "channel" => 21])->field(true)->limit(8)->select();
//        $this->assign("data_list_weixin_21", $data_list);
//        $data_list = $this->weixinModel->where(["status" => 1, "recom" => 1, "channel" => 4])->field(true)->limit(8)->select();
//        $this->assign("data_list_weixin_4", $data_list);
//        $data_list = $this->weixinModel->where(["status" => 1, "recom" => 1, "channel" => 5])->field(true)->limit(8)->select();
//        $this->assign("data_list_weixin_5", $data_list);
//        $data_list = $this->weixinModel->where(["status" => 1, "recom" => 1, "channel" => 25])->field(true)->limit(8)->select();
//        $this->assign("data_list_weixin_25", $data_list);
//        $data_list = $this->weixinModel->where(["status" => 1, "recom" => 1, "channel" => 19])->field(true)->limit(8)->select();
//        $this->assign("data_list_weixin_19", $data_list);

        //品牌 合作伙伴
//        $cache = cache('data_link_rating_2');
//        if (!$cache || empty($cache)) {
//            $data_list = Db::name("media_links")->where(["status" => 1, "type" => 2, "rating" => 2])->field(true)->order("sort desc")->limit(10)->select();
//            foreach ($data_list as &$row) {
//                //图片附件
//                $result = Attachment::get($row["image"]);
//                if (!empty($result["path"])) $row["image"] = $result["path"];
//            }
//            cache('data_link_rating_2', $data_list, 3600);
//        }
//        $this->assign("data_link_rating_2", cache("data_link_rating_2"));
//        $cache = cache('data_link_rating_3');
//        if (!$cache || empty($cache)) {
//            $data_list = Db::name("media_links")->where(["status" => 1, "type" => 2, "rating" => 3])->field(true)->order("sort desc")->limit(10)->select();
//            foreach ($data_list as &$row) {
//                //图片附件
//                $result = Attachment::get($row["image"]);
//                if (!empty($result["path"])) $row["image"] = $result["path"];
//            }
//            cache('data_link_rating_3', $data_list, 3600);
//        }
//        $this->assign("data_link_rating_3", cache("data_link_rating_3"));
        $data_list = Db::name("media_posts")->where('status=1 AND img<>0')->order("istop desc,id desc")->limit(4)->select();
        foreach ($data_list as &$row) {
            //图片附件
//            $result = Attachment::get($row["img"]);
//            if (!empty($result["path"])) $row["img"] = $result["path"];
        }
        $this->assign("data_postspic",$data_list);

        $cache = cache('data_posts');
        if (!$cache || empty($cache)) {
            $data_list = Db::name("media_posts")->where( 'status=1 AND img=0')->order("istop desc,id desc")->limit(5)->select();
            foreach ($data_list as &$row) {
                //图片附件
//                $result = Attachment::get($row["img"]);
//                if (!empty($result["path"])) $row["img"] = $result["path"];
//                $row["name"] = TermsModel::get($row["channel"])["name"];
            }
            cache('data_posts', $data_list, 3600);
        }
        $this->assign("data_posts", cache("data_posts"));
        //友情链接
        $data_list = Db::name("media_links")->where(["status" => 1, "type" => 1])->field(true)->order("sort desc")->limit(100)->select();
        $this->assign("data_link", $data_list);

        $this->assign("pay", config("pay_type"));
//        $this->assign("channel", config("media_channel"));
        $this->assign("p_uid", input("param.ref", 0));//推广来源
        
        return $this->fetch();
    }

    public function media()
    {
        $map["updown"] = 1;
        $map["status"] = 1;
        if (!empty(input("param.channel"))) $map["channel"] = input("param.channel");
        if (!empty(input("param.door"))) $map["door"] = input("param.door");
        if (!empty(input("param.area"))) $map["area"] = input("param.area");
        if (!empty(input("param.newssource"))) $map["newssource"] = input("param.newssource") - 1;
        if (!empty(input("param.position"))) $map["position"] = input("param.position");
        if (!empty(input("param.linktype"))) $map["linktype"] = input("param.linktype");
        if (!empty(input("param.weekfa"))) $map["weekfa"] = (input("param.weekfa") - 1);
        if (!empty(input("param.includecondition"))) $map["includecondition"] = (input("param.includecondition") - 1);
        if (!empty(input("param.keywords"))) $map["name"] = array('like', '%' . urldecode(input("param.keywords")) . '%');
        $sort = "number1 asc";
        switch (input("param.sort")) {
            case "10":
                $sort = "memberprice asc";
                break;
            case "11":
                $sort = "memberprice desc";
                break;
            case "20":
                $sort = "publishrate asc";
                break;
            case "21":
                $sort = "publishrate desc";
                break;
            case "30":
                $sort = "pcweight asc";
                break;
            case "31":
                $sort = "pcweight desc";
                break;
            case "40":
                $sort = "mweight asc";
                break;
            case "41":
                $sort = "mweight desc";
                break;
            case "50":
                $sort = "publicationtime asc";
                break;
            case "51":
                $sort = "publicationtime desc";
                break;
        }
        //print_r($channel_list);exit();
        if (!count(request()->param())) {
            //不带任何参数缓存数据列表
            $result = cache('media_data');
            if (!$result || empty($result)) {
                $result = Db::name("media_media")->where($map)->field(true)->order('number1 asc')->paginate(40);
                cache('media_data', $result, 3600);
            }
            $data_list = cache("media_data");
        } else {
            $data_list = Db::name("media_media")->where($map)->field(true)->order($sort)->paginate(40, false, [
                'query' => ['keywords' => input("param.keywords")]
            ]);
        }
//        print_r($config["media_channel"]);
//        exit();
        $default_sel = [0 => ""];
        $channel_list = $default_sel + $this->config["media_channel"];
        $door_list = $default_sel + $this->config["media_door"];
        $area_list = $default_sel + $this->config["media_area"];
        $entry_list = $default_sel + $this->config["media_entry"];
        $allow_list = $default_sel + $this->config["media_allow"];
        $linktype_list = $default_sel + $this->config["media_linktype"];
        $newssource_list = $this->config["media_newssource"];
        $position_list = $default_sel + $this->config["media_position"];
        $publicationtime_list = [0 => "",
            1 => "一小时内",
            2 => "两小时内",
            12 => "半天",
            24 => "当天",
            48 => "隔天",
            49 => "2天以上"];
        //遍历posts遍历的数据
        foreach ($data_list as &$row) {
            try {
                $row['channel_name'] = $channel_list[$row['channel']];
                $row['door_name'] = $door_list[$row['door']];
                $row['area_name'] = $area_list[$row['area']];
                $row['entry_name'] = $entry_list[$row['entry']];
                $row['allow_name'] = $allow_list[$row['allow']];
                $row['linktype_name'] = $linktype_list[$row['linktype']];
                $row['newssource_name'] = $newssource_list[$row['newssource']];
                $row['position_name'] = $position_list[$row['position']];
                $row['publicationtime_name'] = $publicationtime_list[$row['publicationtime']];
            } catch (Exception $e) {
            }
        }
        $this->assign("data_list", $data_list);
        $this->assign("channel", $this->config["media_channel"]);
        $this->assign("door", $this->config["media_door"]);
        $this->assign("area", $this->config["media_area"]);
        $this->assign("newssource", $this->config["media_newssource"]);
        $this->assign("position", $this->config["media_position"]);
        $this->assign("linktype", $this->config["media_linktype"]);
        $this->assign("weekfa", [0 => "不可发", 1 => "可发"]);

        $this->assign("pkeywords", input("param.keywords"));
        $this->assign("pchannel", input("param.channel"));
        $this->assign("pdoor", input("param.door"));
        $this->assign("parea", input("param.area"));
        $this->assign("pnewssource", input("param.newssource"));
        $this->assign("pposition", input("param.position"));
        $this->assign("plinktype", input("param.linktype"));
        $this->assign("pweekfa", input("param.weekfa"));
        $this->assign("pincludecondition", input("param.includecondition"));
        $this->assign("current_user", $this->auth->islogin());
        return $this->fetch();
    }

    public function mediadetail()
    {
        $id=input("param.id");
        $info = Db::name("media_media")->where(["id" => $id])->find();
        $publicationtime_list = [0 => "",
            1 => "一小时内",
            2 => "两小时内",
            12 => "半天",
            24 => "当天",
            48 => "隔天",
            49 => "2天以上"];
        $info["publicationtime_name"] = $publicationtime_list[$info['publicationtime']];
        if (empty($info)) {
            $this->error("数据不存在！");
            exit();
        }
        if ($info["seo_keywords"] == "屏蔽详情页") {
            echo "该媒体页无法展示！";
            exit();
        }

//        $info["photo"] = Attachment::get($info["photo"])["path"] ?: "/uploads/picture/2018-02-02/5a73bbc878149.jpg";
        $this->assign("info", $info);
        //
        $data_list = Db::name("media_media")->where(['channel' => $info["channel"], 'recom' => 1])->order("id desc")->limit(18)->select();
        foreach ($data_list as &$row) {
            //图片附件
//            $result = Attachment::get($row["photo"]);
//            if (!empty($result["path"])) $row["photo"] = $result["path"];
        }
        $this->assign("recom_list", $data_list);
        //
        //$data_list = Db::name("media_media")->where(['channel' => $info["channel"]])->order("RAND()")->limit(10)->select();
        $data_list = Db::query("SELECT a.* FROM `fa_media_media` AS a JOIN ( SELECT MAX( ID ) AS ID FROM `fa_media_media` ) AS b ON ( a.ID >= FLOOR( b.ID * RAND( ) ) ) LIMIT 20");
        $this->assign("rand_list", $data_list);
        //
        $data_list = Db::name("media_media")->where(['channel' => $info["channel"], 'recom' => 2])->order("id desc")->limit(10)->select();
        $this->assign("channel_list", $data_list);

        $this->assign("channel", [0 => ""] + $this->config["media_channel"]);
        $this->assign("door", [0 => ""] + $this->config["media_door"]);
        $this->assign("area", [0 => ""] + $this->config["media_area"]);
        $this->assign("newssource", [0 => ""] + $this->config["media_newssource"]);
        $this->assign("entry", [0 => ""] + $this->config["media_entry"]);
        $this->assign("is", [0 => "否", 1 => "是"]);

        return $this->fetch();
    }

}
